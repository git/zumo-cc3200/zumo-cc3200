#
# ======== ZumoCC3200 Makefile ========
# This makfile builds README.html from README.md and optionally builds and
# uploads selected Energia sketches to the ZumoCC3200.
#

all: README.html

%.html:%.md
	rm -f $@
	etc/md2html.ksh $< > $@
clean:
	rm -f README.html
	rm -rf build

# ======== Sketch Build and Upload Rules ========
#
#  To Build Sketches
#    Windows
#      Start a git bash shell, cd to the folder containing this makefile, and
#      enter the following commands
#        export ENERGIA=c:/ti/energia-0101E0017
#        $ENERGIA/tools/common/bin/make test
#
#    Linux/Mac
#      Start a  command shell, cd to the folder containing this makefile, and
#      enter the following commands
#        export ENERGIA=...
#        make test
#
#  To Build and Upload ZumoTest
#    Windows
#        export ENERGIA=c:/ti/energia-0101E0017
#        export SERPORT=9
#        $ENERGIA/tools/common/bin/make upload
#
#    Linux/Mac
#        export ENERGIA=...
#        export SERPORT=...
#        make upload
#

# ENERGIA - the installation directory of Energia.  This is only used when
#           building sketches from the command line
# SPORT   - the serial port id used by the flash programmer to upload
#           binaries to the ZumoCC3200
#
ENERGIA ?= c:/ti/energia-0101E0017
SERPORT ?= 9

INO2CPP = $(CURDIR)/etc/ino2cpp
BUILD   = $(INO2CPP)/ebuild.sh -t cc3200emt:RedBearLab_CC3200 -e $(ENERGIA) 
FLASH   = $(CURDIR)/etc/rbload.sh

test: ./build/ZumoTest/ZumoTest.cpp.bin ./build/ManualDrive/ManualDrive.cpp.bin

# a self-contained sketch (on need for ZumoCC3200 library)
./build/ZumoTest/ZumoTest.cpp.bin: $(wildcard src/Energia/ZumoTest/*)
	@echo build system test ...
	$(BUILD) ./src/Energia/ZumoTest ./build/ZumoTest

# a simple sketch that requires the ZumoCC3200 library
./build/ManualDrive/ManualDrive.cpp.bin: $(wildcard ./src/Energia/libraries/ZumoCC3200/examples/ManualDrive/*)
	@echo build a library example ...
	$(BUILD) ./src/Energia/libraries/ZumoCC3200/examples/ManualDrive ./build/ManualDrive -Dsketchbook.path=$(CURDIR)/src/Energia

# upload ZumoTest to the ZumoCC3200
upload: ./build/ZumoTest/ZumoTest.cpp.bin
	@echo flashing $(<F) via serial port $(SERPORT) ...
	$(FLASH) $< $(SERPORT) $(ENERGIA)
