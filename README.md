[![download](etc/build.svg)](https://git.ti.com/zumo-cc3200/zumo-cc3200/archive-tarball/master)

#[Zumo CC3200][ZumoCC3200]

The Zumo CC3200 project was created to enable makers to easily create
applications that leverage the power of a [TI CC3200][CC3200] connected to a
motorized hardware platform equipped with Inertial Measurement (IMU) 
sensors.

This repository provides an Energia library, ZumoCC3200, that provides core
functions that make it possible to create applications with very little code
and without requiring in-depth knowledge of the CC3200 or the specific IMU
sensors used to enable closed-loop motor control.  As with most libraries, 
ZumoCC3200 includes numerous examples that serve as starting points for
new projects.

Each of the ZumoCC3200 examples consists of two programs: an Energia program
that runs on the Zumo CC3200 robot and a [Processing][Processing]
sketch that runs on your laptop. These two programs communicate with one
another via WiFi, enabling you to view telemetry data and control the bot
from your laptop.

##[The ZumoCC3200 Library][ZumoCC3200Folder]

To use the ZumoCC3200 library, 

* [Download the ZumoCC3200 library][ZumoCC3200Download], 
* [Install the ZumoCC3200 library][ZumoCC3200LibInstall] into your Energia
  sketchbook, and
* [Build and run an example][ZumoCC3200Build]

Like most Energia/Arduino libraries, the ZumoCC3200 library includes serveral
examples that serve as simple starting points for your project.

##[Processing Sketches][ProcessingFolder]

The Processing sketches that communicate with the ZumoCC3200 bot are also 
[included in this repo][ProcessingFolder].  Note that some of these sketches
rely on freely available Contributed libraries that must be installed.  

Prerequisite libraries and installation instructions for these sketches is
[here][ProcessingLibInstall].

##[License](license.html)
All of the files in this repo are Open Source and most are licensed under 
either a BSD 3-clause or MIT license.  However, one file does come with a
GPL-2.0 license.  

Complete license details are provided [here](license.html).

##Contributing

[Zumo CC3200][ZumoCC3200] is a collaborative project originally created as a part of the TI Santa Barbara 2015 Summer Intern program by [Adam Dai and Tony Oliverio][Credits], and you are invited to help. 

In-depth details about contributing code, bug fixes, and documentation are
forthcoming.  

[CC3200]: http://www.ti.com/product/cc3200
[Credits]: http://processors.wiki.ti.com/index.php/ZumoCC3200#Credits "Interns"

[Processing]: https:/processing.org "Processing Home Page"
[ProcessingFolder]: https://gitorious.design.ti.com/sb/zumo/trees/master/src/Processing "ZumoCC3200 Processing Sketch Sources"
[ProcessingLibInstall]: http://processors.wiki.ti.com/index.php/ZumoCC3200Demos#Install_Processing_Libraries "Processing Library Install Instructions"

[ZumoCC3200]: http://processors.wiki.ti.com/index.php/ZumoCC3200#The_Zumo_CC3200 "ZumoCC3200 Home Page"
[ZumoCC3200Build]: http://processors.wiki.ti.com/index.php/ZumoCC3200Demos#Build_and_Upload_a_Zumo_CC3200_Example "ZumoCC3200 Energia Build Instructions"
[ZumoCC3200Download]: https://git.ti.com/zumo-cc3200/zumo-cc3200/archive-tarball/master "ZumoCC3200 Repo Download"
[ZumoCC3200Folder]: https://gitorious.design.ti.com/sb/zumo/trees/master/src/Energia/libraries "ZumoCC3200 Energia Library Sources"
[ZumoCC3200LibInstall]: http://processors.wiki.ti.com/index.php/ZumoCC3200Demos#Install_the_Energia_ZumoCC3200_Library "ZumoCC3200 Energia Library Install Instructions"
