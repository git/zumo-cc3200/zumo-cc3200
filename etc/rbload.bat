@echo off
setlocal

set binpath=%~dp0\..\build

if "%1"=="" (
    echo usage: %~nx0 binary [[com_port_id] [energia_install_dir]]
    exit /b 1
)

set bin=%1
if not exist %1 (
    if exist %binpath%\%1 (
        set bin=%binpath%\%1
    ) else (
        echo error: can't find the binary file "%1"
	exit /b 1
    )
)

set comid=%2
if "%2"=="" (
    set comid=9
)

if "%3"=="" (
    set eidir=C:\ti
) else (
    set eidir=%3
)

for /f "usebackq delims=" %%f in (`dir /b /o:n %eidir%\energia-*`) do (
    REM cc3200prog must also be run from it's home directory (!)
    echo chdir %eidir%\%%f\hardware\tools\lm4f\bin
    chdir %eidir%\%%f\hardware\tools\lm4f\bin

    echo cc3200prog %comid% %bin%
    cc3200prog %comid% %bin% || exit /b 1
    echo Upload succeeded.
    echo Push the reset button on the ZumoCC3200 to start the sketch.
    exit /b 0
)

echo error: energia is not installed in %eidir%
exit /b 1
