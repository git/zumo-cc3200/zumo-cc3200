#!/bin/sh
#
# Flash the specified binary on the RedBear CC3200
#
# Usage: rbload binary [[com_port_id] [energia_install_dir]]"
#

if [ $# -lt 1 ]; then
    echo "usage: `basename $0` binary [[com_port_id] [energia_install_dir]]"
    exit 1
fi

# locate the binary file to load
binpath=`dirname $0`/../build
bin="$1"
if [ ! -r "$1" ]; then
    if [ -r "$binpath/$bin" ]; then
        bin="$binpath/$bin"
    else
        echo "error: can't find the binary file $bin"
	exit 1
    fi
fi

# set serial port id
comid="$2"
if [ "$comid" = "" ]; then
    comid=9
fi

# set Energia install dir
eidir="$3"
if [ "$eidir" = "" ]; then
    eidir="C:/ti"
fi

# convert bin to absolute path
bindir="`dirname $bin`"
bindir="`cd $bindir; pwd`"
bin="$bindir/`basename $bin`"

# use the latest version of energia to load $bin
for f in `ls -dt "$eidir"/energia-* 2> /dev/null` "$eidir"; do
    # cc3200prog must be run from it's home directory (!)
    cd "$f/hardware/tools/lm4f/bin"
    ./cc3200prog "$comid" "$bin" || exit 1
    echo Upload succeeded.  
    echo Push the reset button on the ZumoCC3200 to start the sketch.
    exit 0
done
    
echo "error: energia is not installed in $eidir"
exit 1
