#!/bin/sh
#
# Convert a markdown file to an html file
#
# usage: md2html <markdown_file>
#
# html content is output to stdout.
#
# github style sheet from: 
#    https://github.com/sindresorhus/github-markdown-css
# Markdown.pl is from:
#    http://daringfireball.net/projects/downloads/Markdown_1.0.1.zip
#
echo '<!DOCTYPE html>'
echo '<html xmlns="http://www.w3.org/1999/xhtml">'
echo '  <head>'
echo "    <title>Zumo CC3200 `basename $1`</title>"
echo '    <link rel="stylesheet" type="text/css" href="etc/github-markdown.css">'
echo '    <style>'
echo '      .markdown-body {'
echo '         min-width: 200px;'
echo '         max-width: 790px;'
echo '         margin: 0 auto;'
echo '         padding: 30px;'
echo '      }'
echo '    </style>'
echo '  </head>'
echo '  <body ><article class="markdown-body">'
perl `dirname $0`/Markdown.pl $1
echo '  </article></body>'
echo '</html>'

