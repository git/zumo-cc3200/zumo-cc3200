/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * RedBear Zumo Bot interactive control with real-time acceleration display
 * 
 * Before running this program, start the zumo bot and connect to its
 * WiFi network.
 */

import processing.net.Client;
import hypermedia.net.*;

UDP udp;  /* define the UDP object */    

/* IMU data from zumo */
char[] imuBuffer = new char[72];

String[] tokens = new String[12];

char  cmd = ' ';  /* current command */
boolean cont = false;  /* continuously send previous command to get IMU data */

/* zumo IP Address and destination port */
String IP = "192.168.1.1";
int PORT = 8080;

int    MAX_FPS = 30;     /* max frames/sec, i.e., rate of calls to draw() */
int    MAX_XVALS = 80;   /* max # of samples in the x-axis */
int    MAX_PENDING = 6;  /* max # of outstanding commands to send */

String LOG_NAME  = "zumo_log.txt";

/* IMU data graph object */
Graph graph1 = new Graph(150, 80, 400, 200, color (200, 20, 20));

/* IMU data values to graph */
float[] time = { 0 };       
float[] ax   = { 0 }; 
float[] ay   = { 0 };
float[] az   = { 0 };

char curGraph = 'A';
int curGraphOffset = 1;
float curTime = 0;
boolean scaleData = true;

PrintWriter log = null;  /* optional data log */

/*
 *  ======== setup ========
 */
void setup() 
{
    /* create minimal text output window for acc data */
    size(650, 350); 
   
    graph1.xLabel = " Time (s)";
    graph1.yLabel = "Acceleration (m/s^2)";
    graph1.Title  = " Acceleration: (x, y, z) vs. t ";  
    graph1.yMin   = 0;
    graph1.yMax   = 0;

    /* slow the draw() rate down to MAX_FPS frames/sec */
    frameRate(MAX_FPS);
        
    /* create a new datagram connection on port 8080 */
    udp = new UDP(this, 8080);  
    udp.listen(true);
    
    println(udp.address());
}

/*
 *  ======== draw ========
 */
int loopCount = 0;

void draw() 
{
    /* send server commands based on keyboard input */
    if (cont) {
        
        /* if no key is currently pressed, stop the zumo */
        if(!keyPressed){
          cmd = ' ';
        }
        
        /* send the current command every 4th iteration */
        if (loopCount % 4 == 0) {
            /* send the command to the zumo at 192.168.1.1 */
            udp.send(String.valueOf(cmd), IP, PORT);
        }
        
        /* graph the incoming IMU data */
        if (tokens.length > (2 + curGraphOffset)) {
            newPoint(tokens[0 + curGraphOffset],  /* x value */
                     tokens[1 + curGraphOffset],  /* y value */
                     tokens[2 + curGraphOffset]); /* z value */
        }  

        loopCount++;
    }

}

void keyPressed()
{
    if  (key == 'w' || key == 'a' || key == 's' || key == 'd') {
        cmd = key;
    }
    else if (key == 'x') cont = false;
    else if (key == 'c') cont = true;
    else if (key == 'L') {
        if (log == null) {
            log = createWriter(LOG_NAME);
            println("logging started ...");
        }
    }
    else if (key == 'l') {
        if (log != null) {
            log.flush();
            log.close();
            log = null;
            println("logging stopped.");
        }
    }
    else if (key == 'G') {
        if (curGraph != 'G') {
            curGraph = 'G';
            curGraphOffset = 5;
            graph1.yLabel = "Rotational Speed (deg/s)";
            graph1.Title = " Gyro: (x, y, z) vs. t ";  
            graph1.yMin = 0;
            graph1.yMax = 0;
            ax = new float [] { 0 }; 
            ay = new float [] { 0 };
            az = new float [] { 0 };
            time = new float [] { curTime }; 
        }
    }
    else if (key == 'A') {
        if (curGraph != 'A') {
            curGraph = 'A';
            curGraphOffset = 1;
            graph1.yLabel = "Acceleration (m/s^2)";
            graph1.Title = " Acceleration: (x, y, z) vs. t ";  
            graph1.yMin = 0;
            graph1.yMax = 0;
            ax = new float [] { 0 }; 
            ay = new float [] { 0 };
            az = new float [] { 0 };
            time = new float [] { curTime }; 
        }
    }  
}

/*
 *  ======== newPoint ========
 *  add new point to data arrays and update graph
 */
void newPoint(String x, String y, String z)
{
    /* get next values */
    curTime += 1;
    if (log != null) {
        log.println("A: " + x + " " + y + " " + z);
    }
    
    /* update all data arrays */
    time = pushv(time, curTime);
    ax = pushv(ax, scale(curGraph, x));
    ay = pushv(ay, scale(curGraph, y));
    az = pushv(az, scale(curGraph, z));

    /* update display */
    updatePlots();
}

/*
 *  ======== pushv ========
 *  append new data sample to arr[] and return new array
 */
float[] pushv(float [] arr, float val)
{
    float [] tmp = arr;
    
    if (arr.length < MAX_XVALS) {
        tmp = append(arr, val);
    }
    else {
        /* shift data within arr to make room for new value */
        int i;
        for (i = 1; i < MAX_XVALS; i++) {
            arr[i - 1] = arr[i];
        }
        arr[MAX_XVALS - 1] = val;
    }

    return (tmp);
}

/*
 *  ======== updatePlots ========
 *  redraw IMU graph
 */
void updatePlots()
{
    background(255);

    graph1.xMax = max(time);
    graph1.xMin = min(time);
    graph1.yMax = max(max(max(az), max(ax), max(ay)), graph1.yMax);
    graph1.yMin = min(min(min(az), min(ax), min(ay)), graph1.yMin);
                                                  
    graph1.DrawAxis();
      
    graph1.GraphColor = color(200, 40, 40);  
    graph1.LineGraph(time, ax);      
      
    graph1.GraphColor = color(40, 200, 40);   
    graph1.LineGraph(time, ay);

    graph1.GraphColor = color(40, 40, 200);   
    graph1.LineGraph(time, az);
}

float scale(char type, String value)
{
  if (scaleData) {
    switch (type) {
        case 'A':
            return (float(value) * (2 * 9.80665) / float(32768)); /* acc data is +-2G */

        case 'G':
            return (float(value) * 250 / float(32768));           /* gyro data is +-250 deg/sec */
    }
  }
    return (float(value));
}

void receive(byte[] data, String ip, int port) {  
       
  /* parse IMU data */
  String input = new String(imuBuffer);
  tokens = splitTokens(input, " ");
  
}
