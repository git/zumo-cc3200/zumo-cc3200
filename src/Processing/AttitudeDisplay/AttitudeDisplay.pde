/**
 * Attitude Display
 * by Daniel Shiffman.  
 * 
 * Demonstration some basic vector math: subtraction, normalization, scaling
 * Normalizing a vector sets its length to 1.
 */

import processing.net.Client;

Client client;           /* data socket connected to zumo IMU data server */

int    MAX_FPS = 30;     /* max frames/sec, i.e., rate of calls to draw() */
int    MAX_XVALS = 256;  /* max # of samples in the x-axis */
int    MAX_PENDING = 6;  /* max # of outstanding commands to send */

float viewAngle[] = {25, -35};  /* intial viewing angle */
int view = 1;                  /* which view to display */
String[] viewStr = {"Global & Body Frame", "Body Frame in Global Coordinates", "Global Frame in Body's Coordinates"};

/* IMU and DCM data from zumo */
char[] imuBuffer = new char[138];

/* Direction cosine matrix scaled by 100 */
float dcm[][] = {{100, 0, 0}, {0, 100, 0}, {0, 0, 100}};
/* acceleration vector scaled by 100 */
float acc[] = {0, 0, 100};

/*
 *  ======== setup ========
 */
void setup() {
  /* setup for 3D display */
  size(750, 550, P3D);
  smooth();

  //ortho(); 

  /* slow the draw() rate down to MAX_FPS frames/sec */
  frameRate(MAX_FPS);

  /* Connect to zumo's command server IP address and port */
  client = new Client(this, "192.168.1.1", 8080);
}

/*
 *  ======== draw ========
 */
boolean cont = false;  /* continuously send previous command to get IMU data */
String  pcmd = " \n";  /* previous command */
int     pending = 0;   /* number of outstanding commands sent to zumo bot */

void draw() {
  background(150);
  lights();  
  fill(0);

    /* send server commands based on keyboard input */
  if (keyPressed || cont) {
    String cmd = pcmd;
    if  (key == ' ' || key == 'w' || key == 'a' || key == 's' || key == 'd') {
      cmd = key + "\n";
    }
    else if (key == 'x') cont = false;
    else if (key == 'c') cont = true;
        
    /* stop sending commands if too many commands are outstanding */
    if (pending < MAX_PENDING) {
      client.write(cmd);
      pcmd = cmd;
      pending++;
    }
  }

  /* Read IMU data from server and display it */
  if (client.available() >= imuBuffer.length) {
    /* decrement pending count so we can send more commands */
    if (--pending < 0) {
      println("Yikes! pending = " + pending);
      pending = 0;
    }

    /* read IMU data */
    for (int i = 0; i < imuBuffer.length; i++) {
      imuBuffer[i] = client.readChar();
    }

    /* parse IMU data and display it */
    String input = new String(imuBuffer);
    String [] tokens = splitTokens(input, " ");

    /* update the dcm variable */
    dcm[0][0] = float(tokens[13]);
    dcm[0][1] = float(tokens[14]);
    dcm[0][2] = float(tokens[15]);
    dcm[1][0] = float(tokens[16]);
    dcm[1][1] = float(tokens[17]);
    dcm[1][2] = float(tokens[18]);
    dcm[2][0] = float(tokens[19]);
    dcm[2][1] = float(tokens[20]);
    dcm[2][2] = float(tokens[21]);
    
//    println(dcm[0]);
//    println(dcm[1]);
//    println(dcm[2]);
    
    /* update the acceleration vector */
    acc[0] = 100 * (float(tokens[1]) * (2 * 9.80665) / float(32768));
    acc[1] = 100 * (float(tokens[2]) * (2 * 9.80665) / float(32768));
    acc[2] = 100 * (float(tokens[3]) * (2 * 9.80665) / float(32768));
  }

  if (mousePressed == true) {
    viewAngle[0] =  (mouseX - width/2 );
    viewAngle[1] = -(mouseY - height/2) ;
  } 

  text("Camera View Angle(click on center and drag): " + viewAngle[0] + " , " + viewAngle[1]+ 
       "\n"+"View:"+viewStr[view]+" (press V to change)"+
       "\n"+"Key: X - blue, Y - red, Z - green", 10, 20);  

  translate(width/2, height/2, 0);

  for (int i=0; i<100; i++) {
    rotateX(viewAngle[1] / 100 * PI/180);    
    rotateY(viewAngle[0] / 100 * PI/180);
  }   

  drawCoordinateSystem();

  drawDCM();
}

void keyPressed() {
  if (key == 'v' || key == 'V') view = (view+1) % 3;
}

void drawCoordinateSystem() {
  float d = 5;
  //draw axis
  strokeWeight(3);
  stroke(0x330000FF);    // X - blue (screen's -X axis)
  line(-300, 0, 0, 300, 0, 0);  
  fill(0x330000FF);  
  beginShape(TRIANGLES);
  vertex(-310, 0, 0); vertex(-300, d, d); vertex(-300, d, -d);
  vertex(-310, 0, 0); vertex(-300, d, -d); vertex(-300, -d, -d);  
  vertex(-310, 0, 0); vertex(-300, -d, -d); vertex(-300, -d, d);    
  vertex(-310, 0, 0); vertex(-300, -d, d); vertex(-300, d, d);      
  endShape();
  stroke(0x33FF0000);    // Y - red (screen's Z axis)
  line(-0, 0, -300, 0, 0, 300);  
  fill(0x33FF0000);  
  beginShape(TRIANGLES);
  vertex(0, 0, 310); vertex(d, d, 300); vertex(d, -d, 300);
  vertex(0, 0, 310); vertex(d, -d, 300); vertex(-d, -d, 300);  
  vertex(0, 0, 310); vertex(-d, -d, 300); vertex(-d, d, 300);    
  vertex(0, 0, 310); vertex(-d, d, 300); vertex(d, d, 300);      
  endShape();  
  stroke(0x3300FF00);    // Z - green (screen's -Y axis)
  line(-0, -300, 0, 0, 300, 0);  
  fill(0x3300FF00);  
  beginShape(TRIANGLES);
  vertex(0, -310, 0); vertex(d, -300, d); vertex(d, -300, -d); 
  vertex(0, -310, 0); vertex(d, -300, -d); vertex(-d, -300, -d);   
  vertex(0, -310, 0); vertex(-d, -300, -d); vertex(-d, -300, d);    
  vertex(0, -310, 0); vertex(-d, -300, d); vertex(d, -300, d);      
  endShape();
}

void drawDCM() {
  //show inverse gravitation vector in Device coordinates
  if (0==view || 2==view) {
    strokeWeight(5); 
    stroke(#000000);   
    drawVector(acc[0], acc[1], acc[2]);


    //show World in Device coordinates
    strokeWeight(3); 
    stroke(#0000FF);  
    drawVector(dcm[0][0], dcm[0][1], dcm[0][2]);
    stroke(#FF0000);    
    drawVector(dcm[1][0], dcm[1][1], dcm[1][2]);
    stroke(#00FF00);    
    drawVector(dcm[2][0], dcm[2][1], dcm[2][2]);
  }

  if (0==view || 1==view) { 
    //show Device in World coordinates (translated (tranposed) DCM matrix)
    strokeWeight(6); 
    stroke(0==view ? 0x330000FF : #0000FF);      // X - blue 
    drawVector(dcm[0][0], dcm[1][0], dcm[2][0]);
    stroke(0==view ? 0x33FF0000 : #FF0000);    // Y - red
    drawVector(dcm[0][1], dcm[1][1], dcm[2][1]);
    stroke(0==view ? 0x3300FF00 : #00FF00);      // Z - green   
    drawVector(dcm[0][2], dcm[1][2], dcm[2][2]);
  }
}

void drawVector(float x, float y, float z) {
  //Convert from DCM to screen coordinate system:
  //DCM(X) = - Screen(X)  
  //DCM(Y) =   Screen(Z)
  //DCM(Z) = - Screen(Y)
  line(0, 0, 0, -x, -z, y);
}

