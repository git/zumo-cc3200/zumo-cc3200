/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * RedBear Zumo Bot interactive control with real-time acceleration display
 * 
 * Before running this program, start the zumo bot and connect to its
 * WiFi network.
 */

import processing.net.Client;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

Client client;           /* data socket connected to zumo IMU data server */

int    MAX_FPS = 30;     /* max frames/sec, i.e., rate of calls to draw() */
int    MAX_XVALS = 80;   /* max # of samples in the x-axis */
int    MAX_PENDING = 6;  /* max # of outstanding commands to send */

ArrayList<Point> pointList = new ArrayList<Point>();

String LOG_NAME  = "zumo_log.txt";

PrintWriter log = null;  /* optional data log */

/*
 *  ======== setup ========
 */
void setup() 
{
    /* create large drawing window for spline */
    size(800, 800); 
    
    smooth();
    background(153, 204, 255);
    noLoop();
    
    fill(0);
    text("Click anywhere to begin spline"
         +"\n"+"Blue point indicates start of spline"
         +"\n"+"Press 'x' to clear, 's' to send spline", 10, 20);  

    /* slow the draw() rate down to MAX_FPS frames/sec */
    frameRate(MAX_FPS);
        
    /* Connect to zumo's command server IP address and port */
    client = new Client(this, "192.168.1.1", 8080);
}

/*
 *  ======== draw ========
 */
boolean cont = false;  /* continuously send previous command to get IMU data */
String  pcmd = " \n";  /* previous command */
int     pending = 0;   /* number of outstanding commands sent to zumo bot */

void draw() 
{
     background(153, 204, 255);
     
     fill(0);
     text("Click anywhere to begin spline"
         +"\n"+"Blue point indicates start of spline"
         +"\n"+"Press 'x' to clear, 's' to send spline", 10, 20);  
  
     noFill();
     stroke(0);
     beginShape();
     if (pointList.size() > 1) {
       for (int i=0; i<pointList.size(); i++) {
         if(i==0 || i==(pointList.size()-1)){
           curveVertex((float) pointList.get(i).getX(), (float) pointList.get(i).getY());
           curveVertex((float) pointList.get(i).getX(), (float) pointList.get(i).getY());
           System.out.println("x: "+pointList.get(i).getX()+" y: "+(height-pointList.get(i).getY()));
         }
         else{
           curveVertex((float) pointList.get(i).getX(), (float) pointList.get(i).getY());
           System.out.println("x: "+pointList.get(i).getX()+" y: "+(height-pointList.get(i).getY()));
         }
       }
     }
     endShape();
     
     noStroke();
     for (int i=0; i<pointList.size(); i++)
     {
       if(i==0){
         fill(0, 0, 255);
         ellipse((float) pointList.get(i).getX(), (float) pointList.get(i).getY(), 10, 10);
       }
       else{
         fill(255, 0, 0);
         ellipse((float) pointList.get(i).getX(), (float) pointList.get(i).getY(), 5, 5);
       }
     }
     
     System.out.println("drawing");
}

/* save mouse-click position as new point to draw */
void mousePressed()
{
    pointList.add(new Point(mouseX, mouseY));
    redraw();
}

void keyPressed()
{
    /* clear the points */
    if(key == 'x'){
       System.out.println("points cleared");
       pointList.clear();
       redraw();
    }
    /* send each coordinate as a null terminated string to the zumo */
    else if(key == 's'){
       System.out.println("sending data");
       for(Point p: pointList){
          /* prefix each point with a 'p' character */ 
          client.write('p');
          client.write(Integer.toString((int) p.getX()));
          client.write('\0');
          
          delay(300);
          
          client.write('p');
          client.write(Integer.toString((int) (height-p.getY())));
          client.write('\0');
          
          delay(300);
       }
       client.write('\n');
    }
}

void delay(int t)
{
    int time = millis();
    while(millis() - time <= t); 
}


