/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * RedBear Zumo Bot interactive control with real-time acceleration display
 * 
 * Before running this program, start the zumo bot and connect to its
 * WiFi network.
 *
 * Download the required G4P library at http://www.lagers.org.uk/g4p/
 */

import processing.net.Client;

import g4p_controls.GButton;
import g4p_controls.GWindow;
import g4p_controls.GCScheme;
import g4p_controls.GEvent;
import g4p_controls.GDropList;
import g4p_controls.GEditableTextControl;

GButton forward, back, left, right, stop, driveStraight, turnAngle,
        accelerometerData, gyroData, magData, gyroAngleData, errorData,
        motorData, continuousGraphingToggle, logToggle, zeroGyro;
GWindow controlWindow, graphSelector;

Client client; /* data socket connected to zumo IMU data server */

/* IMU data from zumo */
char[] imuBuffer = new char[120];

int MAX_FPS = 30;    /* max frames/sec, i.e., rate of calls to draw() */
int MAX_XVALS = 200;  /* max # of samples in the x-axis */
int MAX_PENDING = 6; /* max # of outstanding commands to send */

String LOG_NAME = "zumo_log.txt";
char command = ' ';
char lastCommand = ' ';

boolean buttonPressed = false;
boolean mainWindowLocationSet = false;
volatile boolean textEntered = false;
byte[] driveInfoToSend;
byte[] angleToSend;

/* IMU data graph object */
Graph graph1 = new Graph(430, 230, 475, 275, color(200, 20, 20));

/* IMU data values to graph */
float[] time = {
    0
};
float[] ax = {
    0
};
float[] ay = {
    0
};
float[] az = {
    0
};

char curGraph = 'A';
int curGraphOffset = 1;
boolean scaleData = true;
long startMillis = 0;
float curTime = 0;
byte[] driveCmd;
byte[] angleCmd;
boolean logRunning = false;
PrintWriter log = null; /* optional data log */
int packetNum = 0;

/*
 *  ======== setup ========
 */
void setup()
{
    /* create minimal text output window for acc data */
    size(1100, 600);

    /* Make controls window and buttons */
    forward = new GButton(this, 120, 200, 80, 80, "Forward");
    back = new GButton(this, 120, 300, 80, 80, "Backward");
    left = new GButton(this, 25, 300, 80, 80, "Left");
    right = new GButton(this, 215, 300, 80, 80, "Right");
    stop = new GButton(this, 25, 390, 270, 50, "STOP");
    stop.setLocalColorScheme(GCScheme.RED_SCHEME);

    driveStraight = new GButton(this, 25, 460, 125, 80,
            "Enable Assisted Drive");
    driveStraight.setLocalColorScheme(GCScheme.GREEN_SCHEME);

    turnAngle = new GButton(this, 170, 460, 125, 80, "Turn 90 degrees");
    turnAngle.setLocalColorScheme(GCScheme.CYAN_SCHEME);

    accelerometerData = new GButton(this, 300, 50, 83, 83,
            "Accelerometer Data");
    accelerometerData.setLocalColorScheme(GCScheme.CYAN_SCHEME);
    gyroData = new GButton(this, 400, 50, 83, 83, "Gyro Raw Data");
    magData = new GButton(this, 500, 50, 83, 83, "Magnetometer Data");
    magData.setLocalColorScheme(GCScheme.GREEN_SCHEME);
    gyroAngleData = new GButton(this, 600, 50, 83, 83,
            "Integrated Gyro Angle Data");
    gyroAngleData.setLocalColorScheme(GCScheme.BLUE_SCHEME);
    errorData = new GButton(this, 700, 50, 83, 83, "PID Error Data");
    errorData.setLocalColorScheme(GCScheme.YELLOW_SCHEME);
    motorData = new GButton(this, 800, 50, 83, 83, "Motor Power Data");
    motorData.setLocalColorScheme(GCScheme.RED_SCHEME);

    continuousGraphingToggle = new GButton(this, 900, 50, 83, 83,
            "Continous Data");
    continuousGraphingToggle.setLocalColorScheme(GCScheme.GREEN_SCHEME);

    logToggle = new GButton(this, 85, 40, 100, 100, "Logger turned off");
    logToggle.setLocalColorScheme(GCScheme.RED_SCHEME);
    zeroGyro = new GButton(this, 1000, 50, 83, 83, "Zero Gyro");
    zeroGyro.setLocalColorScheme(GCScheme.BLUE_SCHEME);

    graph1.xLabel = "Time (s)";
    graph1.yLabel = "Acceleration (m/s^2)";
    graph1.Title = "Acceleration: (x, y, z) vs. t";
    graph1.yMin = 0;
    graph1.yMax = 0;

    /* slow the draw() rate down to MAX_FPS frames/sec */
    frameRate(MAX_FPS);
    
    angleToSend = new byte[3];
    driveInfoToSend = new byte[2];
    driveCmd = new byte[4];
    angleCmd = new byte[5];

    /* Connect to zumo's command server IP address and port */
    client = new Client(this, "192.168.1.1", 8080);
}

/*
 *  ======== draw ========
 */
boolean cont = false; /* continuously send previous command to get IMU data */
String pcmd = " \n";  /* previous command */
int pending = 0;      /* number of outstanding commands sent to zumo bot */

void draw()
{
    if (!mainWindowLocationSet) {
        mainWindowLocationSet = true;
        frame.setLocation(600, 300);
    }

    /* send server commands based on keyboard and/or button input */
    if (keyPressed || cont || buttonPressed) {
        if (startMillis == 0) {
            /* capture initial start time */
            startMillis = java.lang.System.currentTimeMillis();
        }
        String cmd = pcmd;

        if (keyPressed) {
            /* handle keyboard input by converting to a button command  */
            char tmp;
            if ((tmp = key2Command()) != '\0') {
                command = tmp;
            }
        }

        if (command == ' ' || command == '.'
                || command == 'w' || command == 's' || command == 'd'
                || command == 'a' || command == 'g' || command == 't'
                || command == 'z') {
            buttonPressed = false;
            
            if (command == 'g') {
                driveCmd[0] = 'g';
                driveCmd[1] = driveInfoToSend[0];
                driveCmd[2] = driveInfoToSend[1];
                driveCmd[3] = '\n';
            }
            else if (command == 't') {
                angleCmd[0] = 't';
                angleCmd[1] = angleToSend[0];
                angleCmd[2] = angleToSend[1];
                angleCmd[3] = angleToSend[2];
                angleCmd[4] = '\n';
            }
            else {
                cmd = command + "\n";
            }
        }
        else if (command == 'L') {
            if (log == null) {
                log = createWriter(LOG_NAME);
                println("logging started ...");
            }
        }
        else if (command == 'l') {
            if (log != null) {
                log.flush();
                log.close();
                log = null;
                println("logging stopped.");
            }
        }
        else if (command == 'G') {
            if (curGraph != 'G') {
                curGraph = 'G';
                curGraphOffset = 5;
                graph1.yLabel = "Rotational Speed (r/s)";
                graph1.Title = " Gyro: (x, y, z) vs. t ";
                graph1.yMin = 0;
                graph1.yMax = 0;
                ax = new float[] { 0 };
                ay = new float[] { 0 };
                az = new float[] { 0 };
                time = new float[] { curTime };
            }
        }
        else if (command == 'A') {
            if (curGraph != 'A') {
                curGraph = 'A';
                curGraphOffset = 1;
                graph1.yLabel = "Acceleration (m/s^2)";
                graph1.Title = " Acceleration: (x, y, z) vs. t ";
                graph1.yMin = 0;
                graph1.yMax = 0;
                ax = new float[] { 0 };
                ay = new float[] { 0 };
                az = new float[] { 0 };
                time = new float[] { curTime };
            }
        }
        else if (command == 'M') {
            if (curGraph != 'M') {
                curGraph = 'M';
                curGraphOffset = 9;
                graph1.yLabel = "Magnetometer Data (normalized)";
                graph1.Title = " Magnetometer: (x, y, z) vs. t ";
                graph1.yMin = -1.0;
                graph1.yMax = 1.0;
                ax = new float[] { 0 };
                ay = new float[] { 0 };
                az = new float[] { 0 };
                time = new float[] { curTime };
            }
        }
        else if (command == 'N') {
            if (curGraph != 'N') {
                curGraph = 'N';
                curGraphOffset = 13;
                graph1.yLabel = "Gyro Angle (degrees)";
                graph1.Title = "Gyro Angle";
                graph1.yMin = -180;
                graph1.yMax = 180;
                ax = new float[] { 0 };
                ay = new float[] { 0 };
                az = new float[] { 0 };
                time = new float[] { curTime };
            }
        }
        else if (command == 'e') {
            if (curGraph != 'e') {
                curGraph = 'e';
                curGraphOffset = 15;
                graph1.yLabel = "Error (degrees)";
                graph1.Title = "Error";
                graph1.yMin = 0;
                graph1.yMax = 0;
                ax = new float[] { 0 };
                ay = new float[] { 0 };
                az = new float[] { 0 };
                time = new float[] { curTime };
            }
        }
        else if (command == 'm') {
            if (curGraph != 'm') {
                curGraph = 'm';
                curGraphOffset = 16;
                graph1.yLabel = "Motor power";
                graph1.Title = "Motor power (red = left, green = right)";
                graph1.yMin = -400;
                graph1.yMax = 400;
                ax = new float[] { 0 };
                ay = new float[] { 0 };
                az = new float[] { 0 };
                time = new float[] { curTime };
            }
        }
        buttonPressed = false;

        /* stop sending commands if too many commands are outstanding */
        if (pending < MAX_PENDING) {
            pending++;
            if (command == 'g') {
                client.write(driveCmd);
                command = '.';
            }
            else if (command == 't') {
                client.write(angleCmd);
                command = '.';
            }
            else if (command == 'z') {
                client.write(cmd);
                command = '.';
            }
            else {
                client.write(cmd);
                pcmd = cmd;
            }
        }
    }

    /* Read IMU data from server and display it */
    if (client.available() >= imuBuffer.length) {

        /* read IMU data */
        packetNum++;
        for (int i = 0; i < imuBuffer.length; i++) {
            imuBuffer[i] = client.readChar();
        }

        /* decrement pending count so we can send more commands */
        if (--pending < 0) {
            // println("Yikes! pending = " + pending);
            pending = 0;
        }

        /* parse IMU data and display it */
        String input = new String(imuBuffer);
        String [] tokens = splitTokens(input, " ");
        if (packetNum % 100 == 0) {
            for (int i = 0; i < tokens.length; i++) {
                print(tokens[i] + " ");
            }
            println("");
            //println(input);
        }
        if (curGraph != 'N') {
            if (tokens.length > (2 + curGraphOffset)) {
                newPoint(tokens[curGraphOffset], /* x value */
                    tokens[1 + curGraphOffset],  /* y value */
                    tokens[2 + curGraphOffset],  /* z value */
                    tokens[18]);                 /* time */
            }
        }
        else if (tokens.length > curGraphOffset) {
            newPoint(Float.toString(Float.parseFloat(tokens[curGraphOffset])),
                    "0",         /* y value */
                    "0",         /* z value */
                    tokens[18]); /* time */
        }
    }
}

/*
 *  ======== key2Command ========
 *  convert key press to an appropriate button command
 */
char key2Command()
{
    if (key == ' ' || key == 'w' || key == 's' || key == 'd' || key == 'a') {
        return (key);
    }
    else if (key == CODED) { /* handle arrow keys */
        switch (keyCode) {
            case UP:
                return ('w');
            case DOWN:
                return ('s');
            case LEFT:
                return ('a');
            case RIGHT:
                return ('d');
        }
    }
    return ('\0');
}

/*
 *  ======== newPoint ========
 *  add new point to data arrays and update graph
 */
void newPoint(String x, String y, String z, String timestamp)
{
    /* get next values */
    curTime = (java.lang.System.currentTimeMillis() - startMillis) / 1000.0;
    if (log != null) {
        log.println("A: " + x + " " + y + " " + z);
    }
    if (curGraph == 'e') {
        time = pushv(time, curTime);
        ax = pushv(ax, scale(curGraph, x));
        ay = pushv(ay, (float) 0);
        az = pushv(az, (float) 0);
    }
    else if (curGraph == 'm') {
        time = pushv(time, curTime);
        ax = pushv(ax, scale(curGraph, x));
        ay = pushv(ay, scale(curGraph, y));
        az = pushv(az, 0.0);
    }
    else {
        /* update all data arrays */
        time = pushv(time, curTime);
        ax = pushv(ax, scale(curGraph, x));
        ay = pushv(ay, scale(curGraph, y));
        az = pushv(az, scale(curGraph, z));
    }

    /* update display */
    updatePlots();
}

/*
 *  ======== pushv ========
 *  append new data sample to arr[] and return new array
 */
float[] pushv(float [] arr, float val)
{
    float [] tmp = arr;

    if (arr.length < MAX_XVALS) {
        tmp = append(arr, val);
    }
    else {
        /* shift data within arr to make room for new value */
        int i;
        for (i = 1; i < MAX_XVALS; i++) {
            arr[i - 1] = arr[i];
        }
        arr[MAX_XVALS - 1] = val;
    }

    return (tmp);
}

/*
 *  ======== updatePlots ========
 *  redraw IMU graph
 */
void updatePlots()
{
    if (curGraph == 'e') {
        //only graph that matters is x (the error)
        background(200);

        graph1.xMax = max(time);
        graph1.xMin = min(time);

        graph1.DrawAxis();
        graph1.GraphColor = color(200, 40, 40);
        graph1.LineGraph(time, ax);
    }
    else if (curGraph == 'm') {
        //data that matters is x and y (left and right motor speeds)
        background(200);

        graph1.xMax = max(time);
        graph1.xMin = min(time);

        graph1.DrawAxis();

        graph1.GraphColor = color(200, 40, 40);
        graph1.LineGraph(time, ax);

        graph1.GraphColor = color(40, 200, 40);
        graph1.LineGraph(time, ay);
    }

    background(200);

    graph1.xMax = max(time);
    graph1.xMin = min(time);
    graph1.yMax = max(max(max(az), max(ax), max(ay)), graph1.yMax);
    graph1.yMin = min(min(min(az), min(ax), min(ay)), graph1.yMin);

    graph1.DrawAxis();

    graph1.GraphColor = color(200, 40, 40);
    graph1.LineGraph(time, ax);

    graph1.GraphColor = color(40, 200, 40);
    graph1.LineGraph(time, ay);

    graph1.GraphColor = color(40, 40, 200);
    graph1.LineGraph(time, az);
}

/*
 *  ======== scale =========
 */
float scale(char type, String value)
{
    if (scaleData) {
        switch (type) {
            case 'A':
                return (float(value) * (2 * 9.80665) / float(32768)); /* acc data is +-2G */

            case 'G':
                return (float(value) * 250 / float(32768));           /* gyro data is +-250 deg/sec */
            
        }
    }
    return (float(value) / float(100));
}

/*
 *  ======== handleButtonEvents ========
 */
boolean imuDrive = false;
public void handleButtonEvents(GButton button, GEvent event) 
{
    buttonPressed = true;

    if (button == forward) {
        if (imuDrive) {
            lastCommand = command;
            processDriveTimeString("50", true);
            command = 'g';
        }
        else {
            command = 'w';
        }
    }
    else if (button == back) {
        if (imuDrive) {
            lastCommand = command;
            processDriveTimeString("50", false);
            command = 'g';
        }
        else {
            command = 's';
        }
    }
    else if (button == right) {
        command = 'd';
    }
    else if (button == left) {
        command = 'a';
    }
    else if (button == stop) {
        command = ' ';
    }
    else if (button == zeroGyro) {
        command = 'z';
    }
    else if (button == driveStraight) {
        if (!imuDrive) {
            driveStraight.setText("Disable Assisted Drive");
            driveStraight.setLocalColorScheme(GCScheme.RED_SCHEME);
        }
        else {
            driveStraight.setText("Enable Assisted Drive");
            driveStraight.setLocalColorScheme(GCScheme.GREEN_SCHEME);
        }
        imuDrive = !imuDrive;
    }
    else if (button == turnAngle) {
        lastCommand = command;
        command = 't';
        processAngleString("90", true);
    }
    else if (button == accelerometerData) {
        command = 'A';
    }
    else if (button == gyroData) {
        command ='G';
    }
    else if (button == magData) {
        command = 'M';
    }
    else if (button == gyroAngleData) {
        command = 'N';
    }
    else if (button == errorData) {
        command = 'e';
    }
    else if (button == motorData) {
        command = 'm';
    }
    else if (button == continuousGraphingToggle) {
        if (!cont) {
            continuousGraphingToggle.setText("Non-continous Data");
            continuousGraphingToggle.setLocalColorScheme(GCScheme.RED_SCHEME);
        }
        else {
            continuousGraphingToggle.setText("Continuous Data");
            continuousGraphingToggle.setLocalColorScheme(GCScheme.GREEN_SCHEME);
        }
        cont = !cont;
    }
    else if (button == logToggle) {
        if (!logRunning) {
            logToggle.setText("Logger started");
            logToggle.setLocalColorScheme(GCScheme.GREEN_SCHEME);
            logRunning = true;
            command = 'l';
        }
        else {
            logToggle.setText("Logger not running");
            logToggle.setLocalColorScheme(GCScheme.RED_SCHEME);
            logRunning = false;
            command = 'L';
        }
    }
}

/*
 *  ======== processDriveTimeString ========
 *  Pack driveInfoToSend array with the data associated with the
 *  "drive straight" button.
 *      driveInfoToSend[0] - 0-127 forward, 128-255 reverse
 *      driveInfoToSend[1] - duration in seconds (between 0 and 255)
 */
public void processDriveTimeString(String input, boolean forward) 
{
    double time = Double.parseDouble(input); //parse the input as a double
    if (time < 0.0) { //can't be less than a second
        time = 0.00;
    }
    else if (time > 255) {
      time = 255;
    }
    
    int tmp = (int)time;
    driveInfoToSend[1] = (byte)tmp;
    driveInfoToSend[0] = (byte)(forward ? 64 : -64);
}

/*
 *  ======== processAngleString ========
 *  Pack angleToSend array with the data associated with the
 *  "turn" button.
 *      angleToSend[0]   - 1 right, 0 left
 *      angleToSend[1:2] - low-byte, high byte of angle
 */
public void processAngleString(String input, boolean right) 
{
    double angle = Double.parseDouble(input);
    if (angle > 360.0) {
        angle = 360.0;
    }

    int ang = (int)angle;
    byte low = (byte)(ang & 0xFF);
    byte high = (byte)((ang >>8) & 0xFF);
    angleToSend[0] = (byte)(right ? 64 : -64);
    angleToSend[1] = low;
    angleToSend[2] = high;
}

/*
 *  ======== controlWindowHandler ========
 */
void controlWindowHandler(GWindow window)
{
    command = ' '; // ensure the tank doesn't continue driving without control
}

/*
 *  ======== handleDropListEvents ========
 */
public void handleDropListEvents(GDropList list, GEvent event)
{
}

/*
 *  ======== handleTextEvents ========
 */
public void handleTextEvents(GEditableTextControl textcontrol, GEvent event)
{
    if (event == GEvent.ENTERED) {
        textEntered = true;
    }
}
