/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * RedBear Zumo Bot interactive control with real-time acceleration display 
 * of multiple bots. Supports data graphing for up to four zumos 
 * 
 * Before running this program, start the all zumo bots and connect to a shared
 * network (i.e. all zumos and the PC are all on the same network)
 */

import processing.net.Client;
import hypermedia.net.*;
import java.util.ArrayList;

UDP udp;  /* define the UDP object */    

/* IMU data from zumo */
char[] imuBuffer = new char[72];

/* parsed IMU data to graph */
String[] data1 = new String[12];
String[] data2 = new String[12];
String[] data3 = new String[12];
String[] data4 = new String[12];

/* list of IP addresses for Zumos that have sent IMU packets 
   the zumo's index in this list corresponds to its graphs number */
String[] zumoIPs = new String[4]; /* an empty string means no zumo connected */

char  cmd = ' ';  /* current command */
boolean cont = false;  /* continuously send previous command to get IMU data */

/* broadcast IP */
String IP = "192.168.1.255";
int PORT = 8080;

int    MAX_FPS = 30;     /* max frames/sec, i.e., rate of calls to draw() */
int    MAX_XVALS = 80;   /* max # of samples in the x-axis */
int    MAX_PENDING = 6;  /* max # of outstanding commands to send */

String LOG_NAME  = "zumo_log.txt";

/* IMU data graph objects */
Graph graph1 = new Graph(150, 80, 300, 150, color (200, 20, 20));
Graph graph2 = new Graph(600, 80, 300, 150, color (200, 20, 20));
Graph graph3 = new Graph(150, 340, 300, 150, color (200, 20, 20));
Graph graph4 = new Graph(600, 340, 300, 150, color (200, 20, 20));

/* IMU data values to graph */
float[] time1 = { 0 };  
float[] time2 = { 0 };
float[] time3 = { 0 };
float[] time4 = { 0 };

float[] x1   = { 0 }; 
float[] y1   = { 0 };
float[] z1   = { 0 };

float[] x2   = { 0 }; 
float[] y2   = { 0 };
float[] z2   = { 0 };

float[] x3   = { 0 }; 
float[] y3   = { 0 };
float[] z3   = { 0 };

float[] x4   = { 0 }; 
float[] y4   = { 0 };
float[] z4   = { 0 };

char curGraph = 'A';
int curGraphOffset = 1;

long startMillis = 0;
float curTime1 = 0;
float curTime2 = 0;
float curTime3 = 0;
float curTime4 = 0;

boolean scaleData = true;

PrintWriter log = null;  /* optional data log */

/*
 *  ======== setup ========
 */
void setup() 
{
    /* text output window for four graphs */
    size(1000, 550); 
    
    /* initialize all graphs */
    graph1.xLabel = " Time (s)";
    graph1.yLabel = "Acceleration (m/s^2)";
    graph1.Title  = " Zumo1";  
    graph1.yMin   = 0;
    graph1.yMax   = 0;
    
    graph2.xLabel = " Time (s)";
    graph2.yLabel = "Acceleration (m/s^2)";
    graph2.Title  = " Zumo2";  
    graph2.yMin   = 0;
    graph2.yMax   = 0;
    
    graph3.xLabel = " Time (s)";
    graph3.yLabel = "Acceleration (m/s^2)";
    graph3.Title  = " Zumo3";  
    graph3.yMin   = 0;
    graph3.yMax   = 0;
    
    graph4.xLabel = " Time (s)";
    graph4.yLabel = "Acceleration (m/s^2)";
    graph4.Title  = " Zumo4";  
    graph4.yMin   = 0;
    graph4.yMax   = 0;

    /* slow the draw() rate down to MAX_FPS frames/sec */
    frameRate(MAX_FPS);
        
    /* create a new datagram connection on port 6000 */
    udp = new UDP(this, 6000);  
    udp.broadcast(true);
    udp.listen(true);
    
    println(udp.address());
}

/*
 *  ======== draw ========
 */
int loopCount = 0;

void draw() 
{
    /* send server commands based on keyboard input */
    if (cont) {
  
        if(!keyPressed){
          cmd = ' ';
        }
        
        if(loopCount % 2 == 0){
          /* broadcast the command every other iteration of draw */
          udp.send(String.valueOf(cmd), IP, PORT);
        }
         
        /* graph the incoming IMU data */
        if (data1.length > (2 + curGraphOffset) && zumoIPs[0] != null) {
          println("graph 1");
          newPoint(data1[0 + curGraphOffset],  /* x value */
                   data1[1 + curGraphOffset],  /* y value */
                   data1[2 + curGraphOffset],  /* z value */
                   1); 
        }
        
        if (data2.length > (2 + curGraphOffset) && zumoIPs[1] != null) {
          println("graph 2");
          newPoint(data2[0 + curGraphOffset],  /* x value */
                   data2[1 + curGraphOffset],  /* y value */
                   data2[2 + curGraphOffset],  /* z value */
                   2); 
        }
        
        if (data3.length > (2 + curGraphOffset) && zumoIPs[2] != null) {
          println("graph 3");
          newPoint(data3[0 + curGraphOffset],  /* x value */
                   data3[1 + curGraphOffset],  /* y value */
                   data3[2 + curGraphOffset],  /* z value */
                   3); 
        }
        
        if (data4.length > (2 + curGraphOffset) && zumoIPs[3] != null) {
          println("graph 4");
            newPoint(data4[0 + curGraphOffset],  /* x value */
                     data4[1 + curGraphOffset],  /* y value */
                     data4[2 + curGraphOffset],  /* z value */
                     4); 
        }
        
        loopCount++;
        
        /* update display */
        updatePlots();
    }

}

void keyPressed()
{
    if (startMillis == 0) {
        /* capture initial start time */
        startMillis = java.lang.System.currentTimeMillis();
    }

    if  (key == 'w' || key == 'a' || key == 's' || key == 'd') {
        cmd = key;
    }
    else if (key == 'x') cont = false;
    else if (key == 'c') cont = true;
    else if (key == 'L') {
        if (log == null) {
            log = createWriter(LOG_NAME);
            println("logging started ...");
        }
    }
    else if (key == 'l') {
        if (log != null) {
            log.flush();
            log.close();
            log = null;
            println("logging stopped.");
        }
    }
    else if (key == 'G') {
        if (curGraph != 'G') {
            curGraph = 'G';
            curGraphOffset = 5;

            graph1.yLabel = "Rotational Speed (deg/s)";
            graph1.yMin = 0;
            graph1.yMax = 0;
            
            graph2.yLabel = "Rotational Speed (deg/s)";
            graph2.yMin = 0;
            graph2.yMax = 0;
            
            graph3.yLabel = "Rotational Speed (deg/s)";
            graph3.yMin = 0;
            graph3.yMax = 0;
            
            graph4.yLabel = "Rotational Speed (deg/s)";
            graph4.yMin = 0;
            graph4.yMax = 0;
           
            x1   = new float [] { 0 }; 
            y1   = new float [] { 0 };
            z1   = new float [] { 0 };
            
            x2   = new float [] { 0 }; 
            y2   = new float [] { 0 };
            z2   = new float [] { 0 };
            
            x3   = new float [] { 0 }; 
            y3   = new float [] { 0 };
            z3   = new float [] { 0 };
            
            x4   = new float [] { 0 }; 
            y4   = new float [] { 0 };
            z4   = new float [] { 0 };
            
            time1 = new float [] { curTime1 }; 
            time2 = new float [] { curTime2 }; 
            time3 = new float [] { curTime3 }; 
            time4 = new float [] { curTime4 }; 
        }
    }
    else if (key == 'A') {
        if (curGraph != 'A') {
            curGraph = 'A';
            curGraphOffset = 1;
            
            graph1.yLabel = "Acceleration (m/s^2)";
            graph1.yMin = 0;
            graph1.yMax = 0;
            
            graph2.yLabel = "Acceleration (m/s^2)";
            graph2.yMin = 0;
            graph2.yMax = 0;
            
            graph3.yLabel = "Acceleration (m/s^2)";
            graph3.yMin = 0;
            graph3.yMax = 0;
            
            graph4.yLabel = "Acceleration (m/s^2)";
            graph4.yMin = 0;
            graph4.yMax = 0;            
           
            x1   = new float [] { 0 }; 
            y1   = new float [] { 0 };
            z1   = new float [] { 0 };
            
            x2   = new float [] { 0 }; 
            y2   = new float [] { 0 };
            z2   = new float [] { 0 };
            
            x3   = new float [] { 0 }; 
            y3   = new float [] { 0 };
            z3   = new float [] { 0 };
            
            x4   = new float [] { 0 }; 
            y4   = new float [] { 0 };
            z4   = new float [] { 0 };
            
            time1 = new float [] { curTime1 }; 
            time2 = new float [] { curTime2 }; 
            time3 = new float [] { curTime3 }; 
            time4 = new float [] { curTime4 };             
        }
    }  
}

/*
 *  ======== newPoint ========
 *  add new point to data arrays and update graph
 */
void newPoint(String x, String y, String z, int graphNum)
{
    /* log data */
    if (log != null) {
        log.println("A: " + x + " " + y + " " + z);
    }
    
    if(x == null){
      x = "0";
    }
    if(y == null){
      y = "0";
    }
    if(z == null){
      z = "0";
    }
    
    println(x + " " + y + " " + z);
    
    /* update all data arrays */
    switch(graphNum){
      case 1:
        curTime1 = (java.lang.System.currentTimeMillis() - startMillis)/1000.0;
        time1 = pushv(time1, curTime1);
        x1 = pushv(x1, scale(curGraph, x));
        y1 = pushv(y1, scale(curGraph, y));
        z1 = pushv(z1, scale(curGraph, z));        
        break;
      case 2:
        curTime2 = (java.lang.System.currentTimeMillis() - startMillis)/1000.0;
        time2 = pushv(time2, curTime2);
        x2 = pushv(x2, scale(curGraph, x));
        y2 = pushv(y2, scale(curGraph, y));
        z2 = pushv(z2, scale(curGraph, z));
        break; 
      case 3:
        curTime3 = (java.lang.System.currentTimeMillis() - startMillis)/1000.0;
        time3 = pushv(time3, curTime3);
        x3 = pushv(x3, scale(curGraph, x));
        y3 = pushv(y3, scale(curGraph, y));
        z3 = pushv(z3, scale(curGraph, z));      
        break;
      case 4:
        curTime4 = (java.lang.System.currentTimeMillis() - startMillis)/1000.0;
        time4 = pushv(time4, curTime4);
        x4 = pushv(x4, scale(curGraph, x));
        y4 = pushv(y4, scale(curGraph, y));
        z4 = pushv(z4, scale(curGraph, z));      
        break;     
    }
}

/*
 *  ======== pushv ========
 *  append new data sample to arr[] and return new array
 */
float[] pushv(float [] arr, float val)
{
    float [] tmp = arr;
    
    if (arr.length < MAX_XVALS) {
        tmp = append(arr, val);
    }
    else {
        /* shift data within arr to make room for new value */
        int i;
        for (i = 1; i < MAX_XVALS; i++) {
            arr[i - 1] = arr[i];
        }
        arr[MAX_XVALS - 1] = val;
    }

    return (tmp);
}

/*
 *  ======== updatePlots ========
 *  redraw IMU graph
 */
void updatePlots()
{
    background(255);
    
    /* update graph1 */
    graph1.xMax = max(time1);
    graph1.xMin = min(time1);
    graph1.yMax = max(max(max(z1), max(x1), max(y1)), graph1.yMax);
    graph1.yMin = min(min(min(z1), min(x1), min(y1)), graph1.yMin);
                                                  
    graph1.DrawAxis();
      
    graph1.GraphColor = color(200, 40, 40);  
    graph1.LineGraph(time1, x1);      
      
    graph1.GraphColor = color(40, 200, 40);   
    graph1.LineGraph(time1, y1);

    graph1.GraphColor = color(40, 40, 200);   
    graph1.LineGraph(time1, z1);
    
    /* update graph2 */
    graph2.xMax = max(time2);
    graph2.xMin = min(time2);
    graph2.yMax = max(max(max(z2), max(x2), max(y2)), graph2.yMax);
    graph2.yMin = min(min(min(z2), min(x2), min(y2)), graph2.yMin);
                                                  
    graph2.DrawAxis();
      
    graph2.GraphColor = color(200, 40, 40);  
    graph2.LineGraph(time2, x2);      
      
    graph2.GraphColor = color(40, 200, 40);   
    graph2.LineGraph(time2, y2);

    graph2.GraphColor = color(40, 40, 200);   
    graph2.LineGraph(time2, z2);
    
    /* update graph3 */
    graph3.xMax = max(time3);
    graph3.xMin = min(time3);
    graph3.yMax = max(max(max(z3), max(x3), max(y3)), graph3.yMax);
    graph3.yMin = min(min(min(z3), min(x3), min(y3)), graph3.yMin);
                                                  
    graph3.DrawAxis();
      
    graph3.GraphColor = color(200, 40, 40);  
    graph3.LineGraph(time3, x3);      
      
    graph3.GraphColor = color(40, 200, 40);   
    graph3.LineGraph(time3, y3);

    graph3.GraphColor = color(40, 40, 200);   
    graph3.LineGraph(time3, z3);    
    
    /* update graph4 */
    graph4.xMax = max(time4);
    graph4.xMin = min(time4);
    graph4.yMax = max(max(max(z4), max(x4), max(y4)), graph4.yMax);
    graph4.yMin = min(min(min(z4), min(x4), min(y4)), graph4.yMin);
                                                  
    graph4.DrawAxis();
      
    graph4.GraphColor = color(200, 40, 40);  
    graph4.LineGraph(time4, x4);      
      
    graph4.GraphColor = color(40, 200, 40);   
    graph4.LineGraph(time4, y4);

    graph4.GraphColor = color(40, 40, 200);   
    graph4.LineGraph(time4, z4);
}

float scale(char type, String value)
{
  if(value != null){
    if (scaleData) {
      switch (type) {
          case 'A':
              return (float(value) * (2 * 9.80665) / float(32768)); /* acc data is +-2G */
  
          case 'G':
              return (float(value) * 250 / float(32768));           /* gyro data is +-250 deg/sec */
      }
    }
    return (float(value));
  }
  else{
    return 0;
  }
}


/*
 *  ======== receive ========
 *  handle UDP receive events
 */
 int[] receiveCount = {0, 0, 0, 0}; /* each active zumo had a receiveCount, if that receiveCount reaches zero
                                       that zumo is deemed inactive is removed from the zumoIPs list */
 
void receive(byte[] data, String ip, int port) {       
  
  //println("received IMU data from Zumo at " + ip + " and " + port);
  
  /* parse IMU data and display it */
  String input = new String(data);
  //println(input);

  for(int i = 0; i < 4; i++){
    /* don't let the receiveCounts go below 0 */
    if(receiveCount[i] > 0){
      receiveCount[i]--;
    }
    /* if receiveCount reaches zero, boot 'em out */
    if(receiveCount[i] == 0){
      if(zumoIPs[i] != null){
        println("zumo "+zumoIPs[i]+" inactive");
        zumoIPs[i] = null;
      }
     
    }
  }
  
  for(int i = 0; i < 4; i++){
    if(!ip.equals(zumoIPs[i])){
      /* check to see if that ip is empty */
      if(zumoIPs[i] == null){
        zumoIPs[i] = ip;
        receiveCount[i] += 10;
        return;
      }
    }
    else{
      /* update that zumo's data array */
      switch(i){
        case 0:
          data1 = splitTokens(input, " ");
          break;
        case 1:  
          data2 = splitTokens(input, " ");
          break;
        case 2:
          data3 = splitTokens(input, " ");
          break;
        case 3:
          data4 = splitTokens(input, " ");
          break;
      }  
      /* increment that zumo's receiveCount */
      receiveCount[i] += 10;
      return; /* exit the method */
    }
  }
  
  
}
