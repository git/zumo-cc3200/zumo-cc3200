/*  ======== SplineDrive.ino ========
 *  This sketch interacts with a Processing application to enable
 *  the Zumo to drive along a spline path specified by the user.
 *   
 *  The Processing client connects to the Zumo's network then sends 
 *  list of points to create a trajectory from. The Zumo then uses
 *  a cubic spline interpolation library to generate trajectory 
 *  (a list of headings), and attempts to follow that trajectory. 
 *
 *  NOTE: Currently there are possible memory issues with large data
 *  sets/spline curves that cause the program to crash mid-calculation
 */

#include <ZumoCC3200.h>
#include <IMUManager.h>
#include <Utilities.h>
#include "MotionPlanner.h"
#include <WiFiClient.h>

/* imu sketch external declarations */
extern IMUManager imu;
extern bool   isZeroing;

/* motor sketch external declarations */
extern Utilities util;

/* AP sketch external declarations */
#define MAIN_LED_PIN LED_LED
extern int targetPoints[64];
extern MotionPlanner mp;

