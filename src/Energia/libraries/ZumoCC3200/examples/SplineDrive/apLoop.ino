/*
 *  ======== apLoop ========
 *  This sketch starts a network and listens on port PORTNUM for
 *  a new set of points from a Processing client.
 *
 *  The name and password of the network and the port number of the server
 *  (always at IP address 192.168.1.1) can be changed below.
 */

#include <WiFi.h>
#include <ti/sysbios/knl/Task.h>
#include <string.h>
#include "MotionPlanner.h"
#include <unistd.h>
#include <cstdio>
#include <cstdlib>
#include <vector>

/* name of the network and its password */
static const char ssid[] = "zumo-spline";
static const char wifipw[] = "password";

/* port number of the server listening for commands at 192.168.1.1 */
#define PORTNUM 8080

MotionPlanner mp;

/* create data server on port PORTNUM */
static WiFiServer server(PORTNUM);

static int readString(WiFiClient client, char *buf);

/*
 *  ======== apSetup ========
 */
void apSetup()
{
    Serial.begin(9600);
    
    /* set priority of this task to be lower than other tasks */
    Task_setPri(Task_self(), 1);

    /* startup a new network and get the first IP address: 192.168.1.1 */
    Serial.print("Starting a new network: "); Serial.println(ssid);
    WiFi.beginNetwork((char *)ssid, (char *)wifipw);
    
    while (WiFi.localIP() == INADDR_NONE) {
        Serial.print(".");
        delay(300);
    }
    
    mp = MotionPlanner();

    /* startup the command server on port PORTNUM */
    server.begin();

    Serial.print("dataserver started on port "); Serial.println(PORTNUM);
}

/*
 *  ======== apLoop ========
 */
void apLoop()
{
    /* Did a client connect/disconnect since the last time we checked? */
    if (WiFi.getTotalDevices() > 0) {

        /* listen for incoming clients */
        WiFiClient client = server.available();
        if (client) {

            /* if there's a client, listen for incoming point data */
            static int buffer[64] = {0};
            static char temp[16] = {0};

            int bufLen = 0;
            
            /* while connected to the client */
            while (client.connected()) {

                /* if there's a byte to read from the client .. */
                if (client.available()) {
                     
                    char c = client.read();  
                    
                    /* if there's a new line, we have a complete set of points */
                    if(c == '\n'){
                        std::vector<int> points(bufLen);
                        for(int i=0; i < bufLen; i++){
                           points[i] = buffer[i];
                           if(i%2==0){
                              Serial.print("(");Serial.print(buffer[i]);Serial.print(",");
                           }
                           else{
                              Serial.print(buffer[i]);Serial.println(")");
                           }
                           
                        } 
                        mp.setPoints(points);
                        mp.generateParameterizedSpline();

                        /* reset command buffer index to get next command */
                        bufLen = 0;
                        memset(buffer, 0, sizeof(buffer));
                        points.clear();                      
                    }
                    /* there's a new point, add it to the buffer */
                    else if(c == 'p'){
                        readString(client, temp); 
                        
                        int newPoint = atoi(temp);
                        Serial.print("new point: ");
                        Serial.println(newPoint);
                        
                        /* never overrun the buffer */
                        if (bufLen >= (int)(sizeof (buffer))) { 
                            bufLen = sizeof (buffer) - 1;
                        }
    
                        buffer[bufLen++] = newPoint;    
      
                        memset(temp, 0, sizeof(temp));                  
                    }

                }

            }

            /* client disconnected or timed out, close the connection */
            client.flush();
            client.stop();

        }

    }

    /* check for new connections 2 times per second */
    delay(500);

}

/*
 *  ======== readString ========
 * read a null-terminated string from the client into a char buffer 
 */
static int readString(WiFiClient client, char *buf){
    for (unsigned int i = 0; ; ) {
      if(client.available()){
        char c = client.read();
        if(c != '\0'){
          buf[i] = c;
        }
        else{
          return 0;
        }
        i++;
      }
    }
}
