#include <vector>
#include <cassert>
#include <math.h>

#include "MotionPlanner.h"

#include <ZumoMotors.h>
#include <PIDController.h>
#include <IMUManager.h>
#include <vector>
#include <spline.h>

MotionPlanner::MotionPlanner()
{
    dataReceived  = false;
    trajCalculated = false;
    iteration = 0;
}

/*
 *  ======== setPoints ========
 *  update the list of points and reset the trajectory and iteration
 *
 */
void MotionPlanner::setPoints(std::vector<int> inputPoints)
{ 
    /* ensure that input set has equal number of x and y values */
    assert(inputPoints.size() % 2 == 0);
  
    iteration = 0;
    points.clear();
    trajectory.clear();
  
    dataReceived = true;
  
    /* convert each pair of entries of inputPoints into a Point and add it to
     * points
     */
    for (int i = 0; i < inputPoints.size(); i += 2) {
        MotionPlanner::Point newPoint = {inputPoints[i], inputPoints[i+1]};
        points.push_back(newPoint);
    }
}

/*
 *  ======== generateSpline ========
 *  Generate a single spline and calculate the corresponding trajectory
 *
 *  Since length is determined by iterations (which translates to drive time),
 *  this algorithm treats each segment of the spline as equal "length" since it
 *  allocates an equal number of iterations to the trajectory vector for each
 *  segment, even if the actual distance between segments is different.
 */
void MotionPlanner::generateSpline()
{
    /* if points vector contains points */
    if (dataReceived) {
        /* manipulate data and feed it into the spline class */
        int length = points.size(); //number of points
    
        int orderedX[length], orderedY[length]; /* x and y points in order of trajectory */
    
        for (int i = 0; i < length; i++) {
            orderedX[i] = points[i].x;
            orderedY[i] = points[i].y;
        }
    
        int sortedX[length], sortedY[length]; /* x is sorted, y corresponds to x */
    
        for (int i = 0; i < length; i++) {
            sortedX[i] = orderedX[i];
            sortedY[i] = orderedY[i];  
        }
    
        /* sort the X and Y data sets */
        slowSort(sortedX, sortedY, length);

        std::vector<double> X(length), Y(length); /* inputs to the spline */
        for (int i =0; i < length; i++) {
            X[i] = (double) sortedX[i];
            Y[i] = (double) sortedY[i];  
        }
    
        /* create the spline */
        tk::spline s;
        s.set_points(X, Y);
    
        /* use spline to calculate a series of desired headings */
    
        /* for each segment of the spline */
        for (int i = 0; i < (length-1); i++) { /* numbers of segments = number of points - 1 */
      
            /* split the segment into *resolution* number of intervals */
            double interval = (orderedX[i+1]-orderedX[i])/((double) RESOLUTION);
      
            /* poll for the discrete time rate of change at evenly spaced intervals */
            for (int j = 0; j < RESOLUTION; j++) { 
                Serial.print(orderedX[i]+j*interval);Serial.print(", ");Serial.println(s(orderedX[i]+j*interval));
        
                double slope = (s(orderedX[i]+(j+1)*interval)-s(orderedX[i]+j*interval))/interval;
                double heading = Utilities::toDegrees(atan2(s(orderedX[i]+(j+1)*interval)-s(orderedX[i]+j*interval),interval));
                trajectory.push_back(Utilities::wrapAngle(heading-90));
            }
            Serial.println("");
        }
        Serial.print("trajectory data points: ");Serial.println(trajectory.size());
        trajCalculated = true;
    }
    else {
        Serial.println("no data to generate spline with"); 
    }
}

/*
 *  ======== followSpline ========
 *  Drive the zumo along the calculated trajectory  
 */
void MotionPlanner::followSpline(ZumoMotors motors, PIDController pid, double rate)
{    
    if (iteration < trajectory.size()) {
        double error = Utilities::wrapAngle(IMUManager::getGyroYaw() - trajectory[iteration]); 
        Serial.print(trajectory[iteration]);Serial.print(", ");Serial.print(IMUManager::getGyroYaw());Serial.print(", ");Serial.println(iteration);
        float power = pid.calculate(error);
        power = Utilities::saturate(power, rate - 1.0, 1.0 - rate);
       
        float lTotal = Utilities::saturate((rate + power) * 400, -400, 400);
        float rTotal = Utilities::saturate((rate - power) * 400, -400, 400);
     
        motors.setLeftSpeed(lTotal);
        motors.setRightSpeed(rTotal); 
        iteration++;
    }
    else {
        motors.setLeftSpeed(0);
        motors.setRightSpeed(0); 
    }
    delay(1);
}

/*
 *  ======== intervalInterpolate ========
 *  Given an interval and two tangents, create a spline
 *
 *  leftPoint.x < rightPoint.x
 */
tk::spline MotionPlanner::intervalInterpolate(
    MotionPlanner::Point leftPoint,
    MotionPlanner::Point rightPoint, 
    double leftTangent, double rightTangent)
{
}

/*
 *  ======== generateTrajectory ========
 *  Calculate a spline for each interval between control points
 */
void MotionPlanner::generateTrajectory()
{
    std::vector<tk::spline> splineChain;
  
    for (int i = 0; i < (points.size()-1); i++) {
        double t1, t2;
     
        tk::spline s;
     
        /* calculate tangents using a finite three-point difference */
     
        /* calculate Catmull-Rom spline tangents */
     
        if (points[i].x < points[i+1].x) {
            s = intervalInterpolate(points[i], points[i+1], t1, t2); 
            splineChain.push_back(s);
        }
        else if (points[i].x > points[i+1].x) {
            s = intervalInterpolate(points[i+1], points[i], t2, t1); 
            splineChain.push_back(s);
        }
        else {
            /* x values are equal */
        }
     
        double segmentLen = dist(points[i], points[i+1]);
     
        /* conversion: 800 pixels = 8 ft = 800 iterations 
           1 pixel = 1 iteration */
    }
}


/*
 *  ======== generateParameterizedSpline ========
 *  Calculate a spline for both X and Y data sets to create a trajectory
 *
 *  Parameterize the X and Y data sets according to a approximated distance
 *  parameter t.
 */
void MotionPlanner::generateParameterizedSpline()
{
    /* if points vector contains points */
    if (dataReceived) {
        /* manipulate data and feed it into the spline class */
        int length = points.size(); /* number of points */

        std::vector<double> X(length), Y(length); /* X and Y coordinates vectors */

        /* initialize the vectors */
        for (int i = 0; i < length; i++) {
            X[i] = (double) points[i].x;
            Y[i] = (double) points[i].y;  
        }

        std::vector<double> t(length); /* vector of parameter t values, depende data set to X and Y splines */
        double splineLength = 0.0; /* total length, sum of individual segment length */
  
        /* approximate the unitized arc length of each segment of the trajectory in order to assign 
           the appropriate distance parameter value t to each point */

        t[0] = 0.0; /* first value is always 0 */

        for (int i = 1; i < length; i++) {

            /* linear apporximation of the segment length */
            double segLen = (double) dist(points[i-1], points[i]);
            splineLength += segLen;
      
            /* i-th t parameter is the current spline length at the end of the i-th segment */
            t[i] = splineLength;
        }
    
    
        /* create the X vs. t spline */
        tk::spline xS;
        xS.set_points(t, X);

        /* create the Y vs. t spline */
        tk::spline yS;
        yS.set_points(t, Y);
    
        /* use splines to calculate a series of desired headings */
    
        /* for every 4 pixels of spline length, add a heading data point to the trajectory vector
           this way, we end up with trajectory vector whose size corrsponds to the length of the spline */
        for (double pos = 0.0; pos < splineLength; pos += INTERVAL_LENGTH) { 

            /* print out the interpolated coordinate */
            //Serial.print(xS(pos));Serial.print(", ");Serial.println(yS(pos));
        
            /* use trig to calculate the angle formed by the discrete rate of change vector WRTO normal */ 
            double heading = Utilities::toDegrees(atan2(yS(pos) - yS(pos - INTERVAL_LENGTH), xS(pos) - xS(pos - INTERVAL_LENGTH)));

            /* shift the heading 90 degrees counter-clockwise so that 0 is facing forward for the zumo */
            trajectory.push_back(Utilities::wrapAngle(heading - 90));
      
        }
        Serial.print("trajectory data points: ");Serial.println(trajectory.size());
        trajCalculated = true;
    }
    else {
        Serial.println("no data to generate spline with"); 
    }
}

bool MotionPlanner::ready()
{
    return dataReceived && trajCalculated;
}

/*
 *  ======== slowSort ========
 *  Used to sort data before creating spline
 *
 *  Sorts vector X into increasing order, and reorders Y's elements to 
 *  correspond to the sorted X
 */
void MotionPlanner::slowSort(int X[], int Y[], int length)
{
    int tempX[length];
    for (int i = 0; i < length; i++) {
        tempX[i] = X[i];
    }
    int tempY[length];
    for (int i = 0; i < length; i++) {
        tempY[i] = Y[i];
    }
    for (int i = 0; i < length; i++) {
        int indexOfMin = 0;
        for (int j = 0; j < length; j++) {
            if (tempX[j] < tempX[indexOfMin]) {
                indexOfMin = j;
            }
        }
        X[i] = tempX[indexOfMin];
        Y[i] = tempY[indexOfMin];
        tempX[indexOfMin] = 1000;
    }
}

double MotionPlanner::dist(MotionPlanner::Point a, MotionPlanner::Point b)
{
    return hypot(a.x - b.x, a.y - b.y);
}
