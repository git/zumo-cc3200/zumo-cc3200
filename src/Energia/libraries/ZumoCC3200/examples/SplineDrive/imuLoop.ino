/*
*  ======== imuLoop ========
*  This sketch simply reads the Zumo IMU sensors at 20Hz and prints
*  the current value once per second to the UART.
*/

#include <Wire.h>

/* Pololu IMU data instance objects */
IMUManager imu;
float angle = 0;
int imuCount = 0;
int dc_offset = 0;
double noise = 0;

bool hasCalibrated = 0;
bool needToZero = false;
bool isZeroing = false;

/*
*  ======== imuSetup ========
*/

void imuSetup()
{
    Serial.begin(9600);
    Serial.println("imuSetup ...");
    Wire.begin();

    imu = IMUManager();

    /* initialize Zumo accelerometer and magnetometer */
    imu.initAccel();
    imu.enableAccelDefault();

    /* initialize Zumo gyro */
    if (!imu.initGyro()) {
        Serial.print("Failed to autodetect gyro type!");
        delay(1000);
    }
    imu.enableGyroDefault();
    Serial.println("imuSetup done.");
    imuCount = 0;
    
    imu.calibrateGyro(2);
    imu.zeroGyroXAxis();
    imu.zeroGyroYAxis();
    imu.zeroGyroZAxis();
    

}

/*
*  ======== imuLoop ========
*/
void imuLoop()
{
    
    /*update IMU data every 50 ms (20 Hz) */
    imu.readGyro();
    imu.readAccel();
    imu.readMag();

    delay(50);

    imuCount++;

}
