#ifndef MotionPlanner_h
#define MotionPlanner_h

#include <vector>

#include <ZumoMotors.h>
#include <spline.h>
#include <PIDController.h>

class MotionPlanner
{
  public:
    struct Point {
        double x;
        double y; 
    };

    MotionPlanner();

    void setPoints(std::vector<int> inputPoints);
    void generateSpline();
    void followSpline(ZumoMotors motors, PIDController pid, double rate);
    tk::spline intervalInterpolate(MotionPlanner::Point leftPoint, MotionPlanner::Point rightPoint, double leftTangent, double rightTangent);
    void generateTrajectory();
    void generateParameterizedSpline();
    bool ready();

  private:
    int iteration; /* current iteration of followSpline */
    std::vector<Point> points;
    std::vector<double> trajectory; /* list of headings to regulate to while driving spline */
    bool dataReceived;
    bool trajCalculated;

    /* private member methods */

    void slowSort(int X[], int Y[], int length); /* sort data before creating spline */
    double dist(MotionPlanner::Point a, MotionPlanner::Point b);

    /* constants */
        
    /* 800 pixels = 8 ft = 800 iterations -> 1 pixel = 1 iteration */
    static const float INTERVAL_LENGTH = 1.0f; 

    /* number of segments to evaulate between each pair of input points */
    static const int RESOLUTION; 
};   

#endif /* MotionPlanner_h */
