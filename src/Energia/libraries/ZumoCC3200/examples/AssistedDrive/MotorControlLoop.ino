/*
 *  ======== motorLoop ========
 *  This sketch controls the motors on the Zumo by polling a global
 *  variable, motorWASD, once per PERIOD milliseconds for one of the
 *  following motor commands:
 *      'w' - drive forward
 *      's' - drive backward
 *      'a' - turn left
 *      'd' - turn right
 *      ' ' - stop
 *
 *  Other sketches or interrupts can control the zumo by simply writing the
 *  desired command to motorWASD.
 */
#include <Energia.h>
#include <math.h>
#include <PIDController.h>
#include <ZumoMotors.h>

#define PERIOD  1           /* period of motor control updates */
#define TEST 0

char motorWASD = ' '; /* current motor drive command */
float targetAngle = 0.0;
bool targetAngleSet = false;

static ZumoMotors motors; /* Zumo motor driver provided by Pololu */

PIDController steerPID(0.01f, 0.0f, 0.0f);
PIDController spinPID(2.0f, 0.0f, 0.0f);

Utilities::MotorInfo motorStatus;
int count = 0;
bool timedOut = false;
Utilities util;
long initTime;

static int   clip(int speed);
static float clip(float speed);
static void  drive(char wasd, int speed, unsigned int duration);
static int   next(int cur, int goal);

/*
 *  ======== motorSetup ========
 */
void motorSetup(void)
{
    Serial.begin(9600);
    Serial.println("motorSetup ...");

    /* initialize the Pololu driver motor library */
    ZumoMotors::setRightSpeed(0);
    ZumoMotors::setLeftSpeed(0);
    motorStatus.leftSpeed = 0;
    motorStatus.rightSpeed = 0;

    Serial.println("motorSetup done.");
    util = Utilities();
}

/*
 *  ======== motorLoop ========
 *
 *  Based on user input, drive open loop or
 *  perform closed-loop maneuvers
 */
void motorLoop(void)
{
    static char lastWASD = ' ';
    if (lastWASD != motorWASD) {
        lastWASD = motorWASD;
        
        Serial.print("new motor cmd: "); Serial.println(motorWASD);
        Serial.print("  is angle set: "); Serial.println(targetAngleSet);
        Serial.print("  target angle: "); Serial.println(targetAngle);
        Serial.print("  timed out   : "); Serial.println(timedOut);
    }

    if (isZeroing) {
        spinPID = PIDController(2.0f, 0.0f, 0.0f);
        steerPID = PIDController(0.01f, 0.0f, 0.0f);
        ZumoMotors::setLeftSpeed(0);
        ZumoMotors::setRightSpeed(0);
        motorStatus.leftSpeed = 0;
        motorStatus.rightSpeed = 0;
        delay(500);
    }
    else {
        switch (motorWASD) {
            case 's':
            case 'w': {
                targetAngleSet = false;
                drive(motorWASD, 250, PERIOD);
                break;
            }

            case 'd':
            case 'a': {
                targetAngleSet = false;
                drive(motorWASD, 100, PERIOD);
                break;
            }

            case 't': {
                turnAngle(angleToTurn, driveRate);
                break;
            }
            case 'g': {
                driveStraight(driveTime, driveRate);
                break;
            }

            default: {
                targetAngleSet = false;
                motorWASD = ' ';
                drive(' ', 0, 10);
                break;
            }
        }
    }
}

/*
 *  ======== clip ========
 */
static int clip(int speed)
{
    if (speed < -400) {
        speed = -400;
    }
    else if (speed > 400) {
        speed = 400;
    }
    return (speed);
}

static float clip(float speed)
{
    if (speed < -400) {
        speed = -400.0;
    }
    else if (speed > 400) {
        speed = 400;
    }
    return (speed);
}

/*
 *  ======== drive ========
 *  Drive motors in the direction and speed specified by wasd and goal
 *
 *  Note: negative goal values imply a reversal of the wasd direction
 */
static void drive(char wasd, int goal, unsigned int duration)
{
    static int leftSpeed = 0;
    static int rightSpeed = 0;
    
    while (duration > 0) {
        duration--;

        /* gradually adjust curent speeds to goal */
        switch (wasd) {
            case ' ': { /* stop */
                leftSpeed = next(leftSpeed, 0);
                rightSpeed = next(rightSpeed, 0);
                break;
            }
            case 'w': { /* forward */
                leftSpeed = next(leftSpeed, goal);
                rightSpeed = next(rightSpeed, goal);
                break;
            }

            case 's': { /* backward */
                leftSpeed = next(leftSpeed, -goal);
                rightSpeed = next(rightSpeed, -goal);
                break;
            }

            case 'd': { /* turn right */
                leftSpeed = next(leftSpeed, goal);
                rightSpeed = next(rightSpeed, -goal);
                break;
            }

            case 'a': { /* turn left */
                leftSpeed = next(leftSpeed, -goal);;
                rightSpeed = next(rightSpeed, goal);
                break;
            }

            default: {
                break;
            }
        }

        /* clip speeds to allowable range */
        leftSpeed = clip(leftSpeed);
        rightSpeed = clip(rightSpeed);
    
        /* set motor speeds */
        ZumoMotors::setLeftSpeed(leftSpeed);
        ZumoMotors::setRightSpeed(rightSpeed);
        motorStatus.leftSpeed = leftSpeed;
        motorStatus.rightSpeed = rightSpeed;

        /* sleep for 1 ms (so duration is in milliseconds) */
        delay(1);
    }
}

/*
 *  ======== next ========
 *  Compute the next motor speed value given the cur speed and a new goal
 */
static int next(int cur, int goal)
{
    int tmp = (goal - cur) * 0.0625f + cur;
    return (tmp == cur ? goal : tmp);
}

/*
 *  ======== turnAngle ========
 *  Turn an angle using a PID controller
 */
void turnAngle(float angle, float rate)
{
    float error;
    float power;

    if (!targetAngleSet) {
        float currHeading = IMUManager::getGyroYaw();
        targetAngle = Utilities::wrapAngle(currHeading + angle);
        targetAngleSet = true;
    }
    else {
        error = Utilities::wrapAngle(IMUManager::getGyroYaw() - targetAngle);
        power = spinPID.calculate(error);
        bool done = Utilities::inRange(error, -0.5f, 0.5f);
        if (done) {
            motorWASD = ' ';
            targetAngleSet = false;
            power = 0;
        }

        /* if power is too low, the bot doesn't turn */
        if (power > 0 && power < 70) {
            power = 70;
        }
        else if (power < 0 && power > -70) {
            power = -70;
        }

        /* if power is higher than 250, a buildup of error in the
         * integrated gyro angle occurs
         */
        if (power > 250) {
            power = 250;
        }
        else if (power < -250) {
            power = -250;
        }

        motorStatus.leftSpeed = power;
        motorStatus.rightSpeed = -power;

        ZumoMotors::setLeftSpeed(power);
        ZumoMotors::setRightSpeed(-power);

        motorStatus.error = error;
        motorStatus.time = (float)millis();
    }

    delay(10);
}

/*
 *  ======== driveStraight ========
 *  Use the gyro and PID control to drive the robot straight
 */
void driveStraight(float numSeconds, float rate)
{
    bool forward = true;
    
    if (rate < 0) {
        forward = false;
        rate = -rate;
    }

    if (!targetAngleSet) {
        float currHeading = IMUManager::getGyroYaw();
        targetAngle = currHeading;
        timedOut = false;
        targetAngleSet = true;
        initTime = millis();
    }
    else {
        float error = util.wrapAngle(IMUManager::getGyroYaw() - targetAngle);
        float power = steerPID.calculate(error);
        power = util.saturate(power, rate - 1.0f, 1.0f - rate);
        float lTotal = clip((rate + power) * 400);
        float rTotal = clip((rate - power) * 400);

        if (!forward) {
            float temp = lTotal;
            lTotal = -rTotal;
            rTotal = -temp;
        }

        if (timedOut) {
            lTotal = 0;
            rTotal = 0;
            motorWASD = ' ';
        }
        else {
            timedOut = ((millis() - initTime) > ((int)(numSeconds * 1000.0f)));
        }

        ZumoMotors::setLeftSpeed(lTotal);
        ZumoMotors::setRightSpeed(rTotal);

        motorStatus.error = error;
        motorStatus.leftSpeed = lTotal;
        motorStatus.rightSpeed = rTotal;
        motorStatus.time = (float)millis();
    }

    delay(10);
}
