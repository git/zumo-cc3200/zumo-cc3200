
/*  ======== AssistedDrive.ino ========
 *  Simple demo of IMU-assisted driving of the Zumo CC3200
 *
 *  This example creates an access point named zumo-drive with password
 *  "password" and a simple command/telemetry server on port 8080.
 *
 *  The command server allows any WiFi client to drive the Zumo using WASD
 *  keystrokes, drive in stright lines and turn 90-degrees (as determined by
 *  IMU sensors), and acquire IMU telemetry data for real-time display by a
 *  client program running on a laptop.
 *
 *  An example of such a client is provided in the ./Processing/AssistedDrive
 *  sub-directory.  This is a Processing application (which
 *  uses the development IDE that inspired the Energia/Wiring development
 *  environment; see https://processing.org).
 */

#include <IMUManager.h>
#include <Utilities.h>

/* imu sketch external declarations */
extern IMUManager imu;
extern bool isZeroing;
extern Utilities::MotorInfo motorStatus;

/* motor sketch external declarations */
extern char motorWASD;
extern bool needToZero;
extern bool readyToRun;

/* AP sketch external declarations */
#define MAIN_LED_PIN RED_LED
extern float driveRate;
extern float driveTime;
extern int angleToTurn;
