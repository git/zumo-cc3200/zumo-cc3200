/*
 *  ======== wifiLoop ========
 *  This sketch starts a network and listens on port PORTNUM for
 *  command that can control the zumo motors.
 *
 *  The name and password of the network and the port number of the server
 *  (always at IP address 192.168.1.1) can be changed below.
 */

#include <WiFi.h>
#include <ti/sysbios/knl/Task.h>

#include <string.h>

/* name of the network and its password */
static const char ssid[] = "zumo-drive";
static const char wifipw[] = "password";

/* port number of the server listening for commands at 192.168.1.1 */
#define PORTNUM 8080

float driveRate;
float driveTime;
int angleToTurn;
bool readyToRun;

/* create data server on port PORTNUM */
static WiFiServer server(PORTNUM);

static void doWASD(char wasd, WiFiClient client);
static int readBuf(WiFiClient client, uint8_t *buf, unsigned int len);

/*
 *  ======== wifiSetup ========
 */
void wifiSetup()
{
    Serial.begin(9600);

    /* set priority of this task to be lower than other tasks */
    Task_setPri(Task_self(), 1);

    /* startup a new network and get the first IP address: 192.168.1.1 */
    Serial.print("Starting a new network: ");
    Serial.println(ssid);
    WiFi.beginNetwork((char *) ssid, (char *) wifipw);
    while (WiFi.localIP() == INADDR_NONE) {
        Serial.print(".");
        delay(300);
    }

    /* startup the command server on port PORTNUM */
    server.begin();
    Serial.print("dataserver started on port ");
    Serial.println(PORTNUM);
    readyToRun = false;
}

/*
 *  ======== readBuf ========
 */
static int readBuf(WiFiClient client, uint8_t *buf, unsigned int len)
{
    /* wait for len characters to arrive */
    for (unsigned int i = 0; client.available() < len; i++) {
        /* yield or sleep for an ever increasing period to save power */
        i > 0 ? delay(i) : Task_yield();

        if (!client.connected()) {
            return -1;
        }
    }
    client.read(buf, len);
    return 0;
}

/*
 *  ======== wifiLoop ========
 */
void wifiLoop()
{
    /* Did a client connect/disconnect since the last time we checked? */
    if (WiFi.getTotalDevices() > 0) {

        /* listen for incoming clients */
        WiFiClient client = server.available();
        if (client) {
            readyToRun = true;

            /* if there's a client, read and process commands */
            static char buffer[64] = { 0 };
            int bufLen = 0;
            doWASD(' ', client);

            /* while connected to the client, read commands and send results */
            while (client.connected()) {

                /* if there's a byte to read from the client .. */
                if (client.available()) {
                    /* copy it to the command buffer, byte at a time */
                    char c = client.read();

                    /* ignore bogus characters */
                    if (c == '\0' || c == '\r') {
                        continue;
                    }

                    /* never overrun the command buffer */
                    if (bufLen >= (int) (sizeof(buffer))) {
                        bufLen = sizeof(buffer) - 1;
                    }
                    buffer[bufLen++] = c;

                    if (c == 'g') {
                        uint8_t cmdBuf[2] = { 0 };
                        if (readBuf(client, cmdBuf, sizeof(cmdBuf)) < 0) {
                            break;
                        }
                        processDriveCommand(cmdBuf);
                        doWASD('g', client);
                    }
                    else if (c == 't') {
                        Serial.println("Received t, going to send IMU data");
                        uint8_t cmdBuf[3] = { 0 };
                        if (readBuf(client, cmdBuf, sizeof(cmdBuf)) < 0) {
                            break;
                        }
                        processTurnCommand(cmdBuf);
                        doWASD('t', client);
                    }

                    /* if there's a new line, we have a complete command */
                    if (c == '\n') {
                        doWASD(buffer[0], client);
                        /* reset command buffer index to get next command */
                        bufLen = 0;
                    }
                }
            }

            /* client disconnected or timed out, close the connection */
            client.flush();
            client.stop();

            /* disconnect => implicitly stop the motor */
            motorWASD = ' ';
        }
    }

    /* check for new connections 2 times per second */
    delay(500);
}

/*
 *  ======== doWASD ========
 */
static void doWASD(char wasd, WiFiClient client)
{
    static char report[124];
    static char lastCmd = ' ';
    
    if (wasd == 'z') {
        if (lastCmd != 'z') {
            needToZero = true;
        }
    }
    lastCmd = wasd;

    if (wasd != '.') {     /* set new motor command */
        motorWASD = wasd == 'z' ? ' ' : wasd;
    }

    int angle = (int16_t)imu.getGyroYaw() * 100;
    int error = motorStatus.error * 100;
    int leftSpeed = motorStatus.leftSpeed * 100;
    int rightSpeed = motorStatus.rightSpeed * 100;
    long time = millis();
    
    int magX = (int16_t)(imu.getMagX() * 100);
    int magY = (int16_t)(imu.getMagY() * 100);
    int magZ = (int16_t)(imu.getMagZ() * 100);

    /* send current IMU data */
    int len = System_snprintf(report, sizeof(report),
                    "A: %6d %6d %6d G: %6d %6d %6d M: %6d %6d %6d I: %6d U: %6d %6d %6d %6d",
                    imu.accel_x, imu.accel_y, imu.accel_z,
                    imu.gyro_x,  imu.gyro_y,  imu.gyro_z,
                    magX,        magY,        magZ,
                    angle,
                    error, leftSpeed, rightSpeed, time);
    if (client.write((unsigned char *)report, 120) != 120) {
        Serial.println("Error: reply failed, status != 120");
    }
}

/*
 *  ======== processDriveCommand ========
 */
static void processDriveCommand(uint8_t cmd[])
{
    driveRate = (int8_t)cmd[0] / 128.0f;
    driveTime = cmd[1];
    Serial.print("drive rate: "); Serial.println(driveRate);
}

/*
 *  ======== processTurnCommand ========
 */
static void processTurnCommand(uint8_t cmd[])
{
    driveRate = (int8_t)cmd[0] / 128.0f;

    int combined = (cmd[2] << 8) | cmd[1];
    if (driveRate < 0) {
        combined *= -1;
    }
    angleToTurn = combined;
    Serial.print("Turn angle: "); Serial.println(angleToTurn);
}
