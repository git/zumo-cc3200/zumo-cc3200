/*
 *  ======== imuLoop ========
 *  This sketch simply reads the Zumo IMU sensors at 200Hz and prints
 *  the current value once per second to the UART.
 */

#include <Wire.h>

/* Pololu IMU data instance objects */
IMUManager imu;

bool needToZero = false; /* set by WiFi to recalibrate Gyro */
bool isZeroing = true;   /* flag indicating we are in "zero'ing mode */

static void calibrateMagnetometer();

/*
 *  ======== sensorSetup ========
 */
void sensorSetup()
{
    Serial.begin(9600);
    Serial.println("sensorSetup ...");

    Wire.begin();
    imu = IMUManager();

    /* initialize Zumo accelerometer and magnetometer */
    imu.initAccel();
    imu.enableAccelDefault();

    /* initialize Zumo gyro */
    if (!imu.initGyro()) {
        Serial.print("Failed to autodetect gyro type!");
        delay(1000);
    }
    imu.enableGyroDefault();
    
    Serial.println("Calibrating gyro for 2 seconds: keep zumo still during calibration period");
    isZeroing = true;
    imu.calibrateGyro(2);
    imu.zeroGyroXAxis();
    imu.zeroGyroYAxis();
    imu.zeroGyroZAxis();
    isZeroing = false;    
    
    //calibrateMagnetometer();

    Serial.println("sensorSetup done.");
}

/*
 *  ======== sensorLoop ========
 */
void sensorLoop()
{
    static int imuCount = 0;

    /* update IMU data every 5 ms (200 Hz) */

    if (needToZero) {
        imu.calibrateGyro(1);
        imu.zeroGyroXAxis();
        imu.zeroGyroYAxis();
        imu.zeroGyroZAxis();
        needToZero = false;
    }

    /* read data from all IMU sensors */
    imu.readGyro();
    imu.readAccel();
    imu.readMag();

    delay(5);
}

/*
 *  ======== calibrateMagnetometer ========
 *  Calibrate the magnetometer 
 */
static void calibrateMagnetometer()
{
    static bool calibrated = 0;
    if (!calibrated) {
        calibrated = 1;
        
        /* calibrate magnetometer */
        Serial.print("Calibrating magnetometer: rotate the zumo along all axes during calibration period ... ");
        
        /* illuminate LED while calibrating */
        digitalWrite(MAIN_LED_PIN, HIGH);
        
        imu.calibrateMagnetometer(200);
        Serial.println("done.");
        
        /* shutoff LED */
        digitalWrite(MAIN_LED_PIN, LOW);
    }
}
