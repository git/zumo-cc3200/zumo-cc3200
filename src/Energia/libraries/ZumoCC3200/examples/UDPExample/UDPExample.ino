/*  ======== basics.ino ========
 *  Simple demo of the CC3200 Zumo platform basics
 *
 *  This example creates an access point named zumo-udp with password
 *  "password" and a UDP socket on port 8080.
 *
 *  The datagram connections listens for incoming packets and interprets
 *  them as motor commands, and send packets containing IMU data.
 */

#include <ZumoCC3200.h>
#include <L3G.h>
#include <LSM303.h>

/* imu sketch external declarations */
extern LSM303 imuCompass; /* acceleration and magnetometer */
extern L3G    imuGyro;    /* gyro data */

/* motor sketch external declarations */
extern char motorWASD;

/* ap sketch external declarations */
#define MAIN_LED_PIN RED_LED

