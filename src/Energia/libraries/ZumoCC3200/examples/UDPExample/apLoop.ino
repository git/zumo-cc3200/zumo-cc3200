/*
 *  ======== apLoop ========
 *  Unicast UDP communicating with Processing
 *
 *  The name and password of the network and the port number of the server
 *  (always at IP address 192.168.1.1) can be changed below.
 */

#include <WiFi.h>
#include <Pushbutton.h>
#include <ti/sysbios/knl/Task.h>

#include <string.h>

/* name of the network and its password */
static const char ssid[] = "zumo-udp";
static const char wifipw[] = "password";

/* port number for UDP communications */
#define PORTNUM 8080

/* IP Address of processing app */
IPAddress IP(192,168,1,2);

static void sendIMUPacket(WiFiUDP Udp, IPAddress ip, int localPort);

WiFiUDP Udp;

/*
 *  ======== apSetup ========
 */
void apSetup()
{
    Serial.begin(9600);

    /* set priority of this task to be lower than other tasks */
    Task_setPri(Task_self(), 1);
   
    /* startup a new network and get the first IP address: 192.168.1.1 */
    Serial.print("Starting a new network: "); Serial.println(ssid);
    WiFi.beginNetwork((char *)ssid, (char *)wifipw);
    while (WiFi.localIP() == INADDR_NONE) {
       Serial.print(".");
       delay(300);
    } 
    Serial.println("Datagram socket opened on port 8080");
    Udp.begin(PORTNUM);
}

/*
 *  ======== apLoop ========
 */
void apLoop()
{
    // if there's data available, read a packet
    int packetSize = Udp.parsePacket();
    if (packetSize){
       static char buffer[64] = {0}; 
      
       Serial.print("Received packet from ");
       IPAddress remoteIp = Udp.remoteIP();
       Serial.print(remoteIp);
       Serial.print(", port ");
       Serial.println(Udp.remotePort());
    
       // read the packet into packetBufffer
       int len = Udp.read(buffer, 64);
       
       if (len > 0) buffer[len] = 0;
       Serial.print("Contents: ");
       Serial.println(buffer);
       
       motorWASD = buffer[0];
       
    }
    
    /* send IMU data */
    sendIMUPacket(Udp, IP, PORTNUM);
}

/*
 *  ======== sendIMUPacket ========
 */
static void sendIMUPacket(WiFiUDP Udp, IPAddress ip, int localPort)
{
    static char report[80];

    /* send current IMU data */
    System_snprintf(report, sizeof(report),
        "A: %6d %6d %6d G: %6d %6d %6d M: %6d %6d %6d",
        imuCompass.a.x, imuCompass.a.y, imuCompass.a.z,
        imuGyro.g.x,    imuGyro.g.y,    imuGyro.g.z, 
        imuCompass.m.x, imuCompass.m.y, imuCompass.m.z);
    
    Udp.beginPacket(ip, localPort);
    Udp.write(report);
    Udp.endPacket();
}

