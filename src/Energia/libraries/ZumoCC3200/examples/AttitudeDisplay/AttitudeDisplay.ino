/*  ======== AttitudeDisplay.ino ========
 *  Example that calculates the direction cosine matrix from IMU data
 *
 *  This example creates an access point named zumo-attitude with password
 *  "password" and a simple command/telemetry server on port 8080.
 *
 *  The command server sends the IMU and DCM data to a WiFi client for 
 *  display purposes, while listening for any motor commands.
 *
 *  An example of such a client are provided in the ./Processing
 *  sub-directory: AttitudeDisplay.  This is a Processing application (which
 *  uses the development IDE that inspired the Energia/Wiring development
 *  environment; see https://processing.org).
 */
#include <IMUManager.h>
#include <DCM.h>

/* imu sketch external declarations */
extern IMUManager imu;
extern DCM_Handle imuDCM;

/* motor sketch external declarations */
extern char motorWASD;

/* main external declarations */
#define MAIN_LED_PIN RED_LED
