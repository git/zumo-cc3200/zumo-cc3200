/*
 *  ======== apLoop ========
 *  This sketch starts a network and listens on port PORTNUM for
 *  command that can control the zumo motors.
 *
 *  The name and password of the network and the port number of the server
 *  (always at IP address 192.168.1.1) can be changed below.
 */

#include <WiFi.h>
#include <DCM.h>

#include <ti/sysbios/knl/Task.h>
#include <string.h>


/* name of the network and its password */
static const char ssid[] = "zumo-attitude";
static const char wifipw[] = "password";

/* port number of the server listening for commands at 192.168.1.1 */
#define PORTNUM 8080

/* create data server on port PORTNUM */
static WiFiServer server(PORTNUM);

static void doWASD(char wasd);
static void writeData(WiFiClient client);

/*
 *  ======== apSetup ========
 */
void apSetup()
{
    Serial.begin(9600);

    /* set priority of this task to be lower than other tasks */
    Task_setPri(Task_self(), 1);

    /* startup a new network and get the first IP address: 192.168.1.1 */
    Serial.print("Starting a new network: "); Serial.println(ssid);
    WiFi.beginNetwork((char *)ssid, (char *)wifipw);
    while (WiFi.localIP() == INADDR_NONE) {
        Serial.print(".");
        delay(300);
    }

    /* startup the command server on port PORTNUM */
    server.begin();
    Serial.print("dataserver started on port "); Serial.println(PORTNUM);
}

/*
 *  ======== apLoop ========
 */
void apLoop()
{
    /* Did a client connect/disconnect since the last time we checked? */
    if (WiFi.getTotalDevices() > 0) {

        /* listen for incoming clients */
        WiFiClient client = server.available();
        if (client) {

            /* if there's a client, read and process commands */
            static char buffer[64] = {0};
            int bufLen = 0;

            /* while connected to the client, read commands and send results */
            while (client.connected()) {
              
                /* if there's a byte to read from the client .. */
                if (client.available()) {
                    /* copy it to the command buffer, byte at a time */
                    char c = client.read();

                    /* ignore bogus characters */
                    if (c == '\0' || c == '\r') continue;
                   
                    /* never overrun the command buffer */
                    if (bufLen >= (int)(sizeof (buffer))) { 
                        bufLen = sizeof (buffer) - 1;
                    }
                    buffer[bufLen++] = c;

                    /* if there's a new line, we have a complete command */
                    if (c == '\n') {
                        doWASD(buffer[0]);
                        writeData(client);
                        /* reset command buffer index to get next command */
                        bufLen = 0;
                    }
                }
                else{
                    //writeData(client);
                    delay(2);
                }
            }

            /* disconnect => implicitly stop the motor */
            motorWASD = ' ';
            
            /* client disconnected or timed out, close the connection */
            client.flush();
            client.stop();
        }
    }

    /* check for new connections 2 times per second */
    delay(500);
}

/*
 *  ======== doWASD ========
 */
static void doWASD(char wasd)
{
    /* set new motor command */
    motorWASD = wasd;
}

/*
 *  ======== writeData ========
 */
static void writeData(WiFiClient client)
{
    static char report[146];
    
    float matrix[3][3];
    DCM_getMatrix(imuDCM, matrix);
    
    /* convert DCM data for ease of tranfer */
    int matrix00 = (int16_t)(matrix[0][0] * 100);
    int matrix01 = (int16_t)(matrix[0][1] * 100);
    int matrix02 = (int16_t)(matrix[0][2] * 100);
    int matrix10 = (int16_t)(matrix[1][0] * 100);
    int matrix11 = (int16_t)(matrix[1][1] * 100);
    int matrix12 = (int16_t)(matrix[1][2] * 100);
    int matrix20 = (int16_t)(matrix[2][0] * 100);
    int matrix21 = (int16_t)(matrix[2][1] * 100);
    int matrix22 = (int16_t)(matrix[2][2] * 100);
    
//    /* print the DCM data to console */
//    Serial.println("--------DCM--------");
//    Serial.print(matrix00);Serial.print(" ");Serial.print(matrix01);Serial.print(" ");Serial.println(matrix02);
//    Serial.print(matrix10);Serial.print(" ");Serial.print(matrix11);Serial.print(" ");Serial.println(matrix12);
//    Serial.print(matrix20);Serial.print(" ");Serial.print(matrix21);Serial.print(" ");Serial.println(matrix22);
//    Serial.println("-------------------");
    
    //Serial.println("***************************");
    //Serial.print(imu.mag_x);Serial.print(" ");Serial.print(imu.mag_y);Serial.print(" ");Serial.println(imu.mag_z);
    //Serial.print(imu.getMagX());Serial.print(" ");Serial.print(imu.getMagY());Serial.print(" ");Serial.println(imu.getMagZ());
    //Serial.println("***************************");

    /* send current IMU data */
    System_snprintf(report, sizeof(report),
      /* accelerometer, gyro,          magnetometer   DCM */
      /* 0   1   2   3  4   5   6   7  8   9   10  11 12  13  14  15  16  17  18  19  20  21 */
        "A: %6d %6d %6d G: %6d %6d %6d M: %6d %6d %6d D: %6d %6d %6d %6d %6d %6d %6d %6d %6d",
                    imu.accel_x, imu.accel_y, imu.accel_z,
                    imu.gyro_x,  imu.gyro_y,  imu.gyro_z, 
                    imu.mag_x,   imu.mag_y,   imu.mag_z,
                    matrix00,    matrix01,    matrix02,
                    matrix10,    matrix11,    matrix12,
                    matrix20,    matrix21,    matrix22);
    if (client.write((unsigned char *)report, 138) != 138) {
        Serial.println("Error: reply failed, status != 138");
    }
 }
