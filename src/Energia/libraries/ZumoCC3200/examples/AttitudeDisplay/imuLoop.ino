/*
*  ======== imuLoop ========
*  This sketch intializes the direction cosine matrix state with initial
*  sensor readings, then continuously reads the sensors at 200 Hz and 
*  updates the DCM based on the sensor data.
*
*  In addition, this sketch implements the magnetometer calibration sequence
*  which normalizes the raw magnetometer data for use in DCM calculations. During
*  setup, an LED will light up, indicating that the calibration sequence is running.
*  rotating the Zumo sufficiently will complete the calibration and the LED will
*  turn off.
*/

#include <Wire.h>
#include <Energia.h>

#include <DCM.h>
#include <IMUManager.h>

/* IMU data instance objects */
IMUManager imu;
DCM_Handle imuDCM;

static void calibrateDCM(DCM_Handle dcm);
static void getIMUData(DCM_Handle dcm);

/*
 *  ======== imuSetup ========
 */
void imuSetup()
{
    Serial.begin(9600);
    Serial.println("imuSetup ...");

    Wire.begin();
    
    imu = IMUManager();

    /* initialize Zumo accelerometer and magnetometer */
    imu.initAccel();
    imu.enableAccelDefault();

    /* initialize Zumo gyro */
    if (!imu.initGyro()) {
        Serial.print("Failed to autodetect gyro type!");
        delay(1000);
    }
    imu.enableGyroDefault();

    /* calibrate the gyro */
    Serial.println("Calibrating gyro for 2 seconds: keep zumo still during calibration period");
    imu.calibrateGyro(2);
    imu.zeroGyroZAxis();
    imu.zeroGyroYAxis();
    imu.zeroGyroZAxis();
    
    /* create a new DCM 'object' with
     * update time 5 ms, and scaling constants (0.03, 0.97, 0.001)
     */
    imuDCM = DCM_create(0.005f, 0.03f, 0.97f, 0.01f);
}

/*
 *  ======== imuLoop ========
 */
void imuLoop()
{
    /* calibrate DCM filters */
    calibrateDCM(imuDCM);
    
    /* update imuDCM's IMU data every 5 ms (200 Hz) */
    getIMUData(imuDCM);
    
    /* re-compute imuDCM's values */
    DCM_update(imuDCM);

    delay(5);
}

/*
 *  ======== calibrateDCM ========
 *  Calibrate the magnetometer and initialize the DCM object (if necessary)
 */
static void calibrateDCM(DCM_Handle dcm)
{
    static bool calibrated = 0;
    if (!calibrated) {
        calibrated = 1;
        
        /* calibrate magnetometer */
        Serial.print("Calibrating magnetometer: rotate the zumo along all axes during calibration period ... ");
        
        /* illuminate LED while calibrating */
        digitalWrite(MAIN_LED_PIN, HIGH);
        
        imu.calibrateMagnetometer(200);
        Serial.println("done.");
        
        /* shutoff LED */
        digitalWrite(MAIN_LED_PIN, LOW);

        /* get initial calibrated IMU data for dcm */
        getIMUData(dcm);

        /* reset the dcm's filters starting from initial estimates */
        DCM_start(dcm);
    }   
}

/*
 *  ======== getIMUData ========
 *  Provide a new reading of the IMU data to the DCM object
 */
static void getIMUData(DCM_Handle dcm)
{
    float gyroX, gyroY, gyroZ;
    
    /* read data from the sensors */
    imu.readGyro();
    imu.readAccel();
    imu.readMag();
    
    /* convert gyro angular velocity from deg/s to rad/s  */
    gyroX = 0.0174532925f * (imu.getGyroX());
    gyroY = 0.0174532925f * (imu.getGyroY());
    gyroZ = 0.0174532925f * (imu.getGyroZ());
    
    /* push initial IMU data into the DCM "filter" */
    DCM_updateAccelData(dcm, imu.getAccelX(), imu.getAccelY(), imu.getAccelZ());
    DCM_updateGyroData(dcm, gyroX, gyroY, gyroZ);
    DCM_updateMagnetoData(dcm, imu.getMagX(), imu.getMagY(), imu.getMagZ());
}

