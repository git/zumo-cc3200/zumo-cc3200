#ifndef MCP_h
#define MCP_h

extern void MCP_init();

extern int MCP_printf(char *message);

#endif /* MCP_h */
