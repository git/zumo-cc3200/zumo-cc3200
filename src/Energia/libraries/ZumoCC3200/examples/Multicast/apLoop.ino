/*
 *  ======== apLoop ========
 *  This sketch waits on either a button press or a 
 *
 *  The name and password of the network and the port number of the server
 *  (always at IP address 192.168.1.1) can be changed below.
 */

#include <WiFi.h>
#include <Pushbutton.h>
#include <ti/sysbios/knl/Task.h>
#include "MCP.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* name of the network and its password */
char ssid[] = "zumo-multicast";
char wifipw[] = "password";

/* shared network */
char netID[] = "TINK-NET";

/* zumo command port */
#define CMD_PORT 8080

/* IMU data port */
#define DATA_PORT 6000

IPAddress broadcastIP(192,168,1,255);

long signalStrength = 0;

/*
 *  ======== apSetup ========
 */
void apSetup()
{
    Serial.begin(9600);

    /* set priority of this task to be lower than other tasks */
    //Task_setPri(Task_self(), 1);
    
    // attempt to connect to Wifi network:
    Serial.print("Connecting to target: ");
    // print the network name (SSID);
    Serial.println(netID); 
    // Connect to target zumo
    //WiFi.begin(ssid, wifipw);
    WiFi.begin(netID);
    while ( WiFi.status() != WL_CONNECTED) {
      // print dots while we wait to connect
      Serial.print(".");
      delay(300);
    }
    
    Serial.println("Connected");
    Serial.println("Waiting for an ip address");
    
    while (WiFi.localIP() == INADDR_NONE) {
      // print dots while we wait for an ip addresss
      Serial.print(".");
      delay(300);
    }
  
    Serial.println("\nIP Address obtained");
    printWifiStatus();
    
    MCP_init();
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
}

/*
 *  ======== apLoop ========
 */
 
void apLoop()
{  
    signalStrength = WiFi.RSSI();
    Serial.println(signalStrength);
    static char buf[16] = {0};
    snprintf(buf, sizeof(buf), "%d", signalStrength);
    MCP_printf(buf); 

    delay(1000);    
}

void printWifiStatus() {
    // print the SSID of the network you're attached to:
    Serial.print("SSID: ");
    Serial.println(WiFi.SSID());

    // print your WiFi IP address:
    IPAddress ip = WiFi.localIP();
    Serial.print("IP Address: ");
    Serial.println(ip);

    // print the received signal strength:
    long rssi = WiFi.RSSI();
    Serial.print("signal strength (RSSI):");
    Serial.print(rssi);
    Serial.println(" dBm");
}
