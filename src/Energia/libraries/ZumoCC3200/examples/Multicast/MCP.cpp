#include <stdio.h>
#include <string.h>
#include <stdarg.h>

#include "MCP.h"
#include <WiFi.h>

/* multicast address and port */
IPAddress MC_IP(226, 1, 1, 1);
int MC_PORT = 1717;

WiFiUDP Udp;

void MCP_init()
{
    Udp.begin(MC_PORT);    
}

int MCP_printf(char *message)
{
    Udp.beginPacket(MC_IP, MC_PORT);
    Udp.write(message);
    Udp.endPacket();
}

