/*  ======== Multicast.ino ========
 *  Simple demo of the CC3200 Zumo platform basics
 *
 *  This example creates an access point named zumo-multicast with password
 *  "password" and a simple command/telemetry server on port 8080.
 */

#include <ZumoCC3200.h>
#include <L3G.h>
#include <LSM303.h>

/* imu sketch external declarations */
extern LSM303 imuCompass; /* acceleration and magnetometer */
extern L3G    imuGyro;    /* gyro data */

/* motor sketch external declarations */
extern char motorWASD;

/* ap sketch external declarations */
#define MAIN_LED_PIN RED_LED
extern long signalStrength;

