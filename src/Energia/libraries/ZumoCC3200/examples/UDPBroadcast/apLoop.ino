/*
 *  ======== apLoop ========
 *  This sketch connects to an open network named "TINK-NET" and listens
 *  for motor control commands over a UDP socket connection. Upon receiving
 *  a command, the sketch sends a IMU data packet as a reply to the sender.
 *
 */

#include <WiFi.h>
#include <Pushbutton.h>
#include <ti/sysbios/knl/Task.h>

#include <string.h>

/* name of the network and its password */
char ssid[] = "TINK-NET";

/* zumo command port */
#define CMD_PORT 8080

/* IMU data port */
#define DATA_PORT 6000

IPAddress broadcastIP(192,168,1,255);

static void sendIMUPacket(WiFiUDP Udp, IPAddress ip, int localPort);

WiFiUDP Udp;

/*
 *  ======== apSetup ========
 */
void apSetup()
{
    Serial.begin(9600);

    /* set priority of this task to be lower than other tasks */
    Task_setPri(Task_self(), 1);
    
    // attempt to connect to Wifi network:
    Serial.print("Attempting to connect to Network named: ");
    // print the network name (SSID);
    Serial.println(ssid); 
    // Connect to open network
    WiFi.begin(ssid);
    while ( WiFi.status() != WL_CONNECTED) {
      // print dots while we wait to connect
      Serial.print(".");
      delay(300);
    }
    
    Serial.println("Connected");
    Serial.println("Waiting for an ip address");
    
    while (WiFi.localIP() == INADDR_NONE) {
      // print dots while we wait for an ip addresss
      Serial.print(".");
      delay(300);
    }
  
    Serial.println("\nIP Address obtained");
    printWifiStatus();
    
    /* socket listening for motor commands */
    Udp.begin(CMD_PORT);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
}

/*
 *  ======== apLoop ========
 */
 
void apLoop()
{

  /* listen for motor commands from group network */
  // if there's data available, read a packet
  int packetSize = Udp.parsePacket();
  if (packetSize){
      static char buffer[16] = {0}; 
          
      Serial.print("Received packet from ");
      IPAddress remoteIp = Udp.remoteIP();
      Serial.print(remoteIp);
      Serial.print(", port ");
      Serial.println(Udp.remotePort());
        
      
      // read the packet into packetBufffer
      int len = Udp.read(buffer, 64);
             
      if (len > 0) buffer[len] = 0;
      Serial.print("Contents: ");
      Serial.println(buffer);
             
      motorWASD = buffer[0];  
      
      /* send back an IMU packet */ 
      sendIMUPacket(Udp, Udp.remoteIP(), Udp.remotePort());     
   }      
           
}

/*
 *  ======== doWASD ========
 */
static void sendIMUPacket(WiFiUDP Udp, IPAddress ip, int localPort)
{
    static char report[80];

    /* send current IMU data */
    System_snprintf(report, sizeof(report),
        "A: %6d %6d %6d G: %6d %6d %6d M: %6d %6d %6d",
        imuCompass.a.x, imuCompass.a.y, imuCompass.a.z,
        imuGyro.g.x,    imuGyro.g.y,    imuGyro.g.z, 
        imuCompass.m.x, imuCompass.m.y, imuCompass.m.z);
    
    Udp.beginPacket(ip, localPort);
    Udp.write(report);
    Udp.endPacket();
}

void printWifiStatus() {
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print your WiFi IP address:
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  // print the received signal strength:
  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
}
