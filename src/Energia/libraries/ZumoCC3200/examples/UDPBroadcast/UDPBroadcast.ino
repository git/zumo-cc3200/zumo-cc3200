/*  ======== UDPBroadcast.ino ========
 *  A platform for controlling mutiple Zumos using a single Processing 
 *  application.
 *
 *  All Zumos involved and the PC running Processing must connect to a shared
 *  network, in this case, the open network "TINK-NET".
 *
 *  Broadcasting the commands using UDP from the Processing side results in
 *  all Zumos currently running the example to simultaneously drive. Additionally,
 *  all Zumos receiving commands will send back their current IMU readings,
 *  allowing the Processing sketch to simultaneously display data from the Zumos.
 *
 */

#include <ZumoCC3200.h>
#include <L3G.h>
#include <LSM303.h>

/* imu sketch external declarations */
extern LSM303 imuCompass; /* acceleration and magnetometer */
extern L3G    imuGyro;    /* gyro data */

/* motor sketch external declarations */
extern char motorWASD;

/* ap sketch external declarations */
#define MAIN_LED_PIN RED_LED

