/*
 *  ======== apLoop ========
 *  This sketch starts a network and listens on port PORTNUM for
 *  command that can control the zumo motors.
 *
 *  The name and password of the network and the port number of the server
 *  (always at IP address 192.168.1.1) can be changed below.
 */
 
#include <WiFi.h>
#include <ti/sysbios/knl/Task.h>
#include <string.h>
#include <stdio.h>

/* name of the network and its password */
static const char ssid[] = "zumo-balance";
static const char wifipw[] = "password";

/* port number of the server listening for commands at 192.168.1.1 */
#define PORTNUM 8080

/* create data server on port PORTNUM */
static WiFiServer server(PORTNUM);
static void doWASD(char cmd[], WiFiClient client);
static void readBuf(WiFiClient client, uint8_t *buf, unsigned int len);
static void readString(WiFiClient client, char *buf);

/*
 *  ======== apSetup ========
 */
void apSetup()
{
    Serial.begin(9600);

    /* set priority of this task to be lower than other tasks */
    Task_setPri(Task_self(), 1);

    /* startup a new network and get the first IP address: 192.168.1.1 */
    Serial.print("Starting a new network: "); Serial.println(ssid);
    WiFi.beginNetwork((char *)ssid, (char *)wifipw);
    while (WiFi.localIP() == INADDR_NONE) {
        Serial.print(".");
        delay(300);
    }

    /* startup the command server on port PORTNUM */
    server.begin();

    Serial.print("dataserver started on port "); Serial.println(PORTNUM);
}

/*
 *  ======== apLoop ========
 */
void apLoop()
{
    /* Did a client connect/disconnect since the last time we checked? */
    if (WiFi.getTotalDevices() > 0) {

        /* listen for incoming clients */
        WiFiClient client = server.available();

        if (client) {

            /* if there's a client, read and process commands */
            static char buffer[64] = {0};

            int bufLen = 0;

            /* while connected to the client, read commands and send results */
            while (client.connected()) {

                /* if there's a byte to read from the client .. */
                if (client.available()) {

                    /* copy it to the command buffer, byte at a time */
                    char c = client.read();

                    /* ignore bogus characters */
                    if (c == '\0' || c == '\r') continue;

                    /* never overrun the command buffer */
                    if (bufLen >= (int)(sizeof (buffer))) { 
                        bufLen = sizeof (buffer) - 1;
                    }

                    buffer[bufLen++] = c;

                    /* if there's a new line, we have a complete command */
                    if (c == '\n') {
                        /* do the command and send status */
                        doWASD(buffer, client);

                        /* reset command buffer index to get next command */
                        bufLen = 0;
                    }
                }
            }

            /* client disconnected or timed out, close the connection */
            client.flush();
            client.stop();

            /* disconnect => implicitly stop the motor */
            motorWASD = ' ';
        }
    }

    /* check for new connections 2 times per second */
    delay(500);
}

/*
 *  ======== doWASD ========
 */
static void doWASD(char cmd[], WiFiClient client)
{
    static char report[104];
    char c = cmd[0];
    
    if (c == 'P') {
        double newP = atof(cmd + 1);
        Serial.print("new p gain: "); Serial.println(newP);
        motorBal.verticalPID.setP(newP);
    }
    else if (c == 'I') {
        double newI = atof(cmd + 1);
        Serial.print("new i gain: "); Serial.println(newI);
        motorBal.verticalPID.setI(newI);
    }
    else if (c == 'D') {
        Serial.print("new d gain: ");
        double newD = atof(cmd + 1);
        Serial.print("new d gain: "); Serial.println(newD);
        motorBal.verticalPID.setD(newD);
    }
    else if (c == 'w' || c == 'a' || c == 's'  || c == 'd' || c == ' '
             || c == 'b' || c == 'B') {
        motorWASD = c;
    }
 
    /* get current IMU data */
    int tiltAngle = (int16_t)(imu.getFilteredTiltAngle() * 100);
    int motorSpeed = (int16_t)motorBal.balanceStatus.motorPower;
    int balanceError = (int16_t)(motorBal.balanceStatus.error * 100);
    int angularVel = (int16_t)(imu.getGyroY() * 100);

    /* send current IMU data */
    int len = System_snprintf(report, sizeof(report),
        "A: %6d %6d %6d G: %6d %6d %6d M: %6d %6d %6d B: %6d %6d %6d",
                    imu.accel_x, imu.accel_y, imu.accel_z,
                    imu.gyro_x,  imu.gyro_y,  imu.gyro_z, 
                    imu.mag_x,   imu.mag_y,   imu.mag_z, 
                    tiltAngle,   motorSpeed,  angularVel);

    if (client.write((unsigned char *)report, 96) != 96) {
        Serial.println("Error: reply failed, status != 96");
    }
}

