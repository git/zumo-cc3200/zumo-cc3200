/*
 *  ======== imuLoop ========
 *  This sketch reads the Zumo IMU sensors at 20Hz and prints
 *  the current value once per second to the UART.
 */

#include <Wire.h>

/* IMU sketch global data */
IMUManager imu;

/*
 *  ======== imuSetup ========
 */
void imuSetup()
{
    Serial.begin(9600);
    Serial.println("imuSetup ...");

    Wire.begin();
    
    imu = IMUManager();

    /* initialize Zumo accelerometer and magnetometer */
    imu.initAccel();
    imu.enableAccelDefault();

    /* initialize Zumo gyro */

    if (!imu.initGyro()) {
        Serial.print("Failed to autodetect gyro type!");
        delay(1000);
    }
    imu.enableGyroDefault();
    Serial.println("sensorSetup done.");
    
    imu.calibrateGyro(2);
    imu.zeroGyroZAxis();
    imu.zeroGyroYAxis();
    imu.zeroGyroZAxis();
    
    imu.readGyro();
    imu.readAccel();
    imu.readMag();
}

/*
 *  ======== imuLoop ========
 */
void imuLoop()
{
    /* update IMU data every 5 ms (200 Hz) */
    imu.readGyro();
    imu.readAccel();
    imu.readMag();

    delay(5);
}
