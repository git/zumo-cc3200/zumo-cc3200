/*  ======== Balancing.ino ========
 *  Project which implements a "Balancer" class with methods to balance 
 *  the zumo horizontally and vertically.
 *  
 *  This example uses a complementary filter on the gyroscope and 
 *  accelerometer data to form an accurate tilt angle reading, then
 *  employs PID feedback control to regulate that angle to 0.
 *  
 *  This example interacts with the Processing application titled 
 *  "Balancing", which retains most of the functionality of the 
 *  basic graphing application but enables the user to start/stop the 
 *  balancing loop and tune the PID gains in real-time.
 */

#include <IMUManager.h>
#include <Balancer.h>

/* imu sketch external declarations */
extern IMUManager imu;

/* motor sketch external declarations */
extern char     motorWASD;
extern Balancer motorBal;
