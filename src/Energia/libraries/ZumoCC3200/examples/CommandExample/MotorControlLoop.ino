/*
 *  ======== motorLoop ========
 *  This sketch controls the motors on the Zumo by polling a global
 *  variable, motorWASD, once per PERIOD milliseconds for one of the
 *  following motor commands:
 *      'w' - drive forward
 *      's' - drive backward
 *      'a' - turn left
 *      'd' - turn right
 *      ' ' - stop
 *
 *  Other sketches or interrupts can control the zumo by simply writing the
 *  desired command to motorWASD.
 */
#include <Energia.h>
#include <math.h>
#include <Utilities.h>
#include <ZumoMotors.h>
#include <CommandManager.h>
#include <TurnAngleCommand.h>
#include <DriveLineCommand.h>
#include <WaitCommand.h>

#define PERIOD  0           /* period of motor control updates */

char motorWASD = ' '; /* current motor drive command */
static ZumoMotors motors; /* Zumo motor driver provided by Pololu */
Utilities util;
CommandManager manager;
Command* turnCommand;
Command* driveCommand;
Command* waitCommand;
Utilities::MotorInfo motorStatus;

/*
 *  ======== motorSetup ========
 */
void motorSetup(void)
{
    Serial.begin(9600);
    Serial.println("motorSetup ...");

    /* initialize the Pololu driver motor library */
    ZumoMotors::setRightSpeed(0);
    ZumoMotors::setLeftSpeed(0);
    /* setup an LED to indcate forward/backward movement or turning */
    pinMode(MAIN_LED_PIN, OUTPUT);

    util = Utilities();
    manager = CommandManager();
    turnCommand = (Command*) new TurnAngleCommand(-90, 2.5f);
    driveCommand = (Command*) new DriveLineCommand(0.5f, 3.0f);
    waitCommand = (Command*) new WaitCommand(1.0f);
    manager.addCommand(driveCommand);
    manager.addCommand(waitCommand);
    manager.addCommand(turnCommand);
    manager.addCommand(waitCommand);
    manager.addCommand(driveCommand);
    manager.addCommand(waitCommand);
    manager.addCommand(turnCommand);
    manager.addCommand(waitCommand);
    manager.addCommand(driveCommand);
    manager.addCommand(waitCommand);
    manager.addCommand(turnCommand);
    manager.addCommand(waitCommand);
    manager.addCommand(driveCommand);
    motorStatus.error = 0;
    motorStatus.leftSpeed = 0;
    motorStatus.rightSpeed = 0;
    motorStatus.time = 0;
    Serial.println("motorSetup done.");
}

/*
 *  ======== motorLoop ========
 *
 *  Based on user input, drive open loop or
 *  perform closed-loop maneuvers
 */
void motorLoop(void)
{
    if (isZeroing) {
        motors.setLeftSpeed(0);
        motors.setRightSpeed(0);
    }

    else {
        manager.run(motorStatus);
    }
    delay(25);
}
