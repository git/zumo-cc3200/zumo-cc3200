/*
 *  ======== imuLoop ========
 *  This sketch simply reads the Zumo IMU sensors at 200Hz and prints
 *  the current value once per second to the UART.
 */

#include <Wire.h>

/* Pololu IMU data instance objects */
IMUManager imu;
float angle = 0;
int dc_offset = 0;
float noise = 0;
bool hasCalibrated = 0;
bool needToZero = false; /* set by WiFi to recalibrate Gyro */
bool isZeroing = true;   /* flag indicating we are in "zero'ing mode */

/*
 *  ======== sensorSetup ========
 */
void sensorSetup()
{
    Serial.begin(9600);
    Serial.println("sensorSetup ...");
    Wire.begin();
    imu = IMUManager();

    /* initialize Zumo accelerometer and magnetometer */
    imu.initAccel();
    imu.enableAccelDefault();

    /* initialize Zumo gyro */
    if (!imu.initGyro()) {
        Serial.print("Failed to autodetect gyro type!");
        delay(1000);
    }
    imu.enableGyroDefault();

    isZeroing = true;
    imu.calibrateGyro(2);
    imu.zeroGyroXAxis();
    imu.zeroGyroYAxis();
    imu.zeroGyroZAxis();
    isZeroing = false;

    Serial.println("sensorSetup done.");
}

/*
 *  ======== sensorLoop ========
 */
void sensorLoop()
{
    static int imuCount = 0;

    /* update IMU data every 10 ms (100 Hz) */
    if (needToZero) {
        imu.zeroGyroZAxis();
        imu.zeroGyroYAxis();
        needToZero = false;
    }

    /* read data from all IMU sensors */
    imu.readGyro();
    imu.readAccel();
    imu.readMag();
    
    delay(10);
    imuCount++;
}
