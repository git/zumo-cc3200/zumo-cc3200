/*  ======== CommandExample.ino ========
 *  Simple demo of the CC3200 Zumo platform
 *
 *  This example creates an access point named zumo-command with password
 *  "password" and a simple command/telemetry server on port 8080.
 */

#include <ZumoMotors.h>
#include <Utilities.h>
#include <IMUManager.h>
#include <WiFiClient.h>

/* imu sketch external declarations */
extern IMUManager imu;
extern bool isZeroing;
extern Utilities::MotorInfo motorStatus;
extern bool needToZero;
extern bool readyToRun;

/* motor sketch external declarations */
extern char motorWASD;
extern float motorInfo[4];


/* AP sketch external declarations */
#define MAIN_LED_PIN RED_LED
extern float driveRate;
extern int angleToTurn;
