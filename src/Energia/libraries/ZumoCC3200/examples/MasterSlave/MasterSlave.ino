/*  ======== MasterSlave.ino ========
 *  Simple demo of the CC3200 Zumo platform basics
 *
 *  This example creates an access point named zumo_group with password
 *  "password" and a simple command/telemetry server on port 8080.
 *
 *  The command server allows any WiFi client to drive the Zumo using WASD
 *  keystrokes and acquire IMU telemetry data for real-time display.
 *
 *  Two examples of such a client are provided in the src/Processing
 *  sub-directory of git@gitorious.design.ti.com:sb/zumo.git: zgraph and
 *  zecho.  Both are Processing applications (the development IDE that
 *  originally inspired the Energia/Wiring development environment; see
 *  https://processing.org).
 */

#include <ZumoCC3200.h>
#include <L3G.h>
#include <LSM303.h>

/* imu sketch external declarations */
extern LSM303 imuCompass; /* acceleration and magnetometer */
extern L3G    imuGyro;    /* gyro data */

/* motor sketch external declarations */
extern char motorWASD;

/* ap sketch external declarations */
#define MAIN_LED_PIN RED_LED

