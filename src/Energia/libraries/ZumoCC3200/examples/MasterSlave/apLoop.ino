/*
 *  ======== apLoop ========
 *  This sketch waits on either a button press or a 
 *
 *  The name and password of the network and the port number of the server
 *  (always at IP address 192.168.1.1) can be changed below.
 */

#include <WiFi.h>
#include <Pushbutton.h>
#include <ti/sysbios/knl/Task.h>

#include <string.h>

/* name of the network and its password */
char ssid[] = "zumo_group";
char wifipw[] = "password";

/* zumo command port */
#define CMD_PORT 8080

/* IMU data port */
#define DATA_PORT 6000

/* Create a Pushbutton object for pin 12 (the Zumo user pushbutton pin) */
Pushbutton button(ZUMO_BUTTON);

IPAddress broadcastIP(192,168,1,255);


static bool waiting = true;

/* 1 is master, 0 is slave, -1 is unassigned */
int rank = -1;

static void sendIMUPacket(WiFiUDP Udp, IPAddress ip, int localPort);

//WiFiUDP UdpRX; /* UDP connection for receiving */ 
//WiFiUDP UdpTX; /* UDP connection for transmitting */

WiFiUDP Udp;

/*
 *  ======== apSetup ========
 */
void apSetup()
{
    Serial.begin(9600);

    /* set priority of this task to be lower than other tasks */
    //Task_setPri(Task_self(), 1);
    
    // attempt to connect to group network:
    Serial.print("Attempting to connect to Network named: ");
    Serial.println(ssid); 
    WiFi.begin(ssid, wifipw);
    
    while (waiting) {
 
      /* if group network is found */
      if(WiFi.status() == WL_CONNECTED){
         Serial.println("Connected to the group network");
         Serial.println("Waiting for an ip address");
  
         while (WiFi.localIP() == INADDR_NONE) {
           // print dots while we wait for an ip addresss
           Serial.print(".");
           delay(300);
         }
         
         Serial.println("\nIP Address obtained");
         printWifiStatus();
         
         /* this zumo is a slave */
         rank = 0;
         
         waiting = false;
      }
      /* if zumo button is pressed */
      else if(button.getSingleDebouncedRelease()){
         Serial.println("Button press detected");
         /* startup a new network and get the first IP address: 192.168.1.1 */
         Serial.print("Starting a new network: "); Serial.println(ssid);
         WiFi.beginNetwork((char *)ssid, (char *)wifipw);
         while (WiFi.localIP() == INADDR_NONE) {
            Serial.print(".");
            delay(300);
         }
         
         /* this zumo is the master */
         rank = 1;
         
         waiting = false;
      }
      else{
         WiFi.begin(ssid, wifipw);
         Serial.print(".");
         delay(300);
      }
    }
    
    Udp.begin(CMD_PORT);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
}

/*
 *  ======== apLoop ========
 */
 
void apLoop()
{
  int packetSize = 0;
  
  switch(rank){
      case -1: /* unassigned case: should theoretically never happen */
        Serial.println("rank was not assigned, please exit the program and try again");
        break;
        
      case 0: /* slave case: listen for motor commands from group network */
        // if there's data available, read a packet
        packetSize = Udp.parsePacket();
        if (packetSize){
           static char buffer[64] = {0}; 
          
           Serial.print("Received packet from ");
           IPAddress remoteIp = Udp.remoteIP();
           Serial.print(remoteIp);
           Serial.print(", port ");
           Serial.println(Udp.remotePort());
        
           /* if packet is from master */
           if(Udp.remotePort() == CMD_PORT){
             // read the packet into packetBufffer
             int len = Udp.read(buffer, 64);
             
             if (len > 0) buffer[len] = 0;
             Serial.print("Contents: ");
             Serial.println(buffer);
             
             motorWASD = buffer[0];  
           }  
           
        }      
        break;
        
      case 1: /* master case: listen for motor commands from processing, and multicast them over the group network */
        // if there's data available, read a packet
        packetSize = Udp.parsePacket();
        if (packetSize){
           static char buffer[64] = {0}; 
          
           Serial.print("Received packet from ");
           IPAddress remoteIp = Udp.remoteIP();
           Serial.print(remoteIp);
           Serial.print(", port ");
           Serial.println(Udp.remotePort());
           
           /* if packet is from processing */
           if(Udp.remotePort() == DATA_PORT){
             // read the packet into packetBufffer
             int len = Udp.read(buffer, 64);
             
             if (len > 0) buffer[len] = 0;
             Serial.print("Contents: ");
             Serial.println(buffer);
             
             motorWASD = buffer[0];  
             
             /* broadcast the command */
             Udp.beginPacket(broadcastIP, CMD_PORT);
             Udp.write(motorWASD);
             Udp.endPacket();
           }
        }
        /* broadcast the command */
        Udp.beginPacket(broadcastIP, CMD_PORT);
        Udp.write((char *)"hello slaves");
        Udp.endPacket();
        break;
  }
}

/*
 *  ======== doWASD ========
 */
static void sendIMUPacket(WiFiUDP Udp, IPAddress ip, int localPort)
{
    static char report[80];

    /* send current IMU data */
    System_snprintf(report, sizeof(report),
        "A: %6d %6d %6d G: %6d %6d %6d M: %6d %6d %6d",
        imuCompass.a.x, imuCompass.a.y, imuCompass.a.z,
        imuGyro.g.x,    imuGyro.g.y,    imuGyro.g.z, 
        imuCompass.m.x, imuCompass.m.y, imuCompass.m.z);
    
    Udp.beginPacket(ip, localPort);
    Udp.write(report);
    Udp.endPacket();
}

void printWifiStatus() {
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print your WiFi IP address:
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  // print the received signal strength:
  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
}
