#ifndef CommandManager_h
#define CommandManager_h

#include <vector>
#include "Command.h"
#include "Utilities.h"

using namespace std;

class CommandManager {
public:
    CommandManager();
    void addCommand(Command* cmd);
    void clearAllCommands();
    void run(Utilities::MotorInfo &info);
private:
    static vector<Command *> commandList;
};
#endif
