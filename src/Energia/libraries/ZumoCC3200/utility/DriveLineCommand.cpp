/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <Energia.h>

#include "Utilities.h"
#include "ZumoMotors.h"
#include "IMUManager.h"

#include "DriveLineCommand.h"

DriveLineCommand::DriveLineCommand(float speed, float timeout)
{
    _speed = speed;
    _forward = _speed > 0;
    steerController = PIDController(0.03, 0.0, 0.0);
    numSeconds = timeout;
    initialized = false;
}

void DriveLineCommand::run(Utilities::MotorInfo &info)
{
    if (!timedOut) {
        float error = Utilities::wrapAngle(
                IMUManager::getGyroYaw() - targetAngle);
        float power = steerController.calculate(error);
        power = Utilities::saturate(power, _speed - (float) 1.0,
                (float) 1.0 - _speed);
        float lTotal = Utilities::clip((_speed + power) * 400);
        float rTotal = Utilities::clip((_speed - power) * 400);
        if (!_forward) {
            float temp = lTotal;
            lTotal = -rTotal;
            rTotal = -temp;
        }
        if (!timedOut) {
            Serial.print("lTotal: ");
            Serial.print(lTotal);
            Serial.print(" rTotal: ");
            Serial.println(rTotal);
            ZumoMotors::setLeftSpeed(lTotal);
            ZumoMotors::setRightSpeed(rTotal);
        }
        else {
            ZumoMotors::setLeftSpeed(0);
            ZumoMotors::setRightSpeed(0);
        }
        timedOut = ((millis() - initTime) > ((int) (numSeconds * 1000.0)));

        info.error = error;
        info.leftSpeed = lTotal;
        info.rightSpeed = rTotal;
        info.time = (float) millis();
    }
}

void DriveLineCommand::initialize()
{
    targetAngle = IMUManager::getGyroYaw();
    initTime = millis();
    initialized = true;
    timedOut = false;
}

void DriveLineCommand::end()
{
    ZumoMotors::setLeftSpeed(0);
    ZumoMotors::setRightSpeed(0);
    timedOut = true;
    initialized = false;
}

bool DriveLineCommand::isFinished()
{
    return timedOut;
}

bool DriveLineCommand::hasBeenInitialized()
{
    return initialized;
}

bool DriveLineCommand::isTimedOut()
{
    return timedOut;
}
