/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef Balancer_h
#define Balancer_h

#include "ZumoMotors.h"
#include "PIDController.h"
#include "IMUManager.h"

class Balancer
{
  public:
    struct BalanceInfo {
    	float error;
    	int motorPower;
    	float PIDTerms[3];
    };

    Balancer();  

    static void horizontalBalance(ZumoMotors motors, IMUManager imu);
    static void verticalBalance(ZumoMotors motors, IMUManager imu);
    static void verticalDrive(ZumoMotors motors, char cmd);
    static void setHorizontalGains(float p, float i, float d);
    static void setVerticalGains(float p, float i, float d);

    static BalanceInfo balanceStatus;
    static PIDController horizontalPID;
    static PIDController verticalPID;

  private:
    /* the angle at which the zumo balances vertically */
    const static float VERTICAL_BALANCING_ANGLE = -1.337f;

    /* the angle at which the zumo balances horizontally */
    const static float HORIZONTAL_BALANCING_ANGLE = -89.0f;
};
#endif
