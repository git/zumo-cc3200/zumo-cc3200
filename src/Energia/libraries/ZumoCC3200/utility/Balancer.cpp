/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== Balancer ========
 *  This class provides methods for balancing the zumo using PID
 *  control as well as modifying the PID gains and accessing 
 *  state information for display and/or debugging purposes
 */

#include "Balancer.h"
#include "PIDController.h"
#include "ZumoMotors.h"
#include "IMUManager.h"

#include <math.h>

PIDController Balancer::horizontalPID;
PIDController Balancer::verticalPID;

Balancer::BalanceInfo Balancer::balanceStatus;

Balancer::Balancer()
{
     horizontalPID = PIDController(10.6f, 0.0f, 0.0f);

     /* The D term for vertical balancing is not calculated by the PID 
      * controller. Instead it is taken directly from the gyro y-axis
      * angular velocity measurements.
      */
     verticalPID = PIDController(27.6f, 0.27f, 0.2f);

     balanceStatus.error = 0;
     balanceStatus.motorPower = 0;
     balanceStatus.PIDTerms[0] = 0;
     balanceStatus.PIDTerms[1] = 0;
     balanceStatus.PIDTerms[2] = 0;
}

/*
 *  ======== horizontalBalance ========
 *  Balance horizontally using PID feedback control
 *
 *  Meant to be used on a pivoting balance board, so 
 *  the robot will use drive actions to find the center
 *  of balance.
 */
void Balancer::horizontalBalance(ZumoMotors motors, IMUManager imu)
{
     float angle = imu.getFilteredTiltAngle();
     
     /* regulate the error to -89 degrees */
     float power = horizontalPID.calculate(HORIZONTAL_BALANCING_ANGLE - angle);

     power = Utilities::saturate(power, -40, 40);
     motors.setLeftSpeed((int)power);
     motors.setRightSpeed((int)power);
}

/*
 *  ======== verticalBalance ========
 *  Balance vertically using PID feedback control
 *
 *  The robot uses its front wheels to stabilize itself
 *  vertically on a medium-friction surface
 */
void Balancer::verticalBalance(ZumoMotors motors, IMUManager imu)
{
     float angle = imu.getFilteredTiltAngle();
     float angularVelocity = imu.getGyroY();
     float error = angle - VERTICAL_BALANCING_ANGLE;
     int power = 0;
     
     /* if the robot hasn't fallen over */
     if (abs(error) < 30) {
         
         /* calculate the reactionary output with the derivative term being
          * calculated separately */
         power = (int) (verticalPID.calculate(error) + verticalPID.getD() * angularVelocity);
         
         /* limit oscillations in the stable regime */
//         if(util.abs(error) < 1.5){
//            power = util.saturate(power, -50, 50); 
//         }
         
         /* if error changes signs drastically, i.e. significant oscillation detected, limit power */
//         if((error * prevError < 0) && util.abs(error-prevError) > 1.2){
//            power = util.saturate(power, -60, 60); 
//         }

     }
     else {
         power = 0;
     }

     motors.setLeftSpeed(power);
     motors.setRightSpeed(power);
     
     /* store new data in balanceInfo */
     balanceStatus.error = error;
     balanceStatus.motorPower = power;
     balanceStatus.PIDTerms[0] = verticalPID.getPContribution();
     balanceStatus.PIDTerms[1] = verticalPID.getIContribution();
     balanceStatus.PIDTerms[2] = verticalPID.getDContribution();
}

/*
 *  ======== verticalDrive ========
 *  Drive the zumo while it is balanced vertically
 *
 *  The method takes in a WASD command and moves in the
 *  corresponding direction while using a PID controller
 *  to balance vertically
 */
void Balancer::verticalDrive(ZumoMotors motors, char cmd)
{
  
  /* unimplemented */
  
}

