/*  ======== ZumoTest.ino ========
 *  Completely self-contained test of the Zumo CC3200
 *
 *  This example creates an access point named zumo-test with password
 *  "password" and a simple command/telemetry server on port 8080.
 *
 *  The command server allows any WiFi client to drive the Zumo using WASD
 *  keystrokes and acquire IMU telemetry data for real-time display by a
 *  client program running on a laptop.
 *
 *  Two examples of such a client are provided in the ../Processing/ManualDrive
 *  sub-directory: zgraph and zecho.  Both are Processing applications (which
 *  uses the development IDE that inspired the Energia/Wiring development
 *  environment; see https://processing.org).
 */
#include "L3G.h"
#include "LSM303.h"

/* imu sketch external declarations */
extern LSM303 imuCompass; /* acceleration and magnetometer */
extern L3G    imuGyro;    /* gyro data */

/* motor sketch external declarations */
extern char motorCmd;
extern int  motorLeft;
extern int  motorRight;

/* main external declarations */
#define MAIN_LED_PIN RED_LED
